$(document).ready(function () {
    $("#table_department").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive:true,
        ajax:'get-departments-ajax',
        columns:[
            {data: 'id', name:'id'},
            {data: 'name', name:'name'},
        ],
        "columnDefs": [{
            "targets": 2,
            "data": "download_link",
            "render": function ( data, type, row, meta ) {
                var icon , title,colorClas;
                if(row.status != 0 )
                {
                    icon = "far fa-check-circle";
                    title = "Desactivar";
                    colorClas ="primary";
                }else{
                    icon = "fas fa-times-circle";
                    title = "Activar";
                    colorClas = "danger";
                }
                return  '<a href="#" class="btn btn-info edit-department"    data-val="'+ row +'"    data-toggle="tooltip" title="Editar usuario"><span class="far fa-edit"></span></span></a> '+
                    '<a href="#" class="btn btn-'+ colorClas +' changeStatus"   data-id ="'+ row.id +'"  data-toggle="tooltip" title="'+ title +'"><span class="' + icon + '"></span></a> '
            }}
        ]
    });

    $(".new-department").on('click',function (e) {
        e.preventDefault();
        $('#form-save-department').trigger("reset");
        $("#modal-department").modal('show');
        $(".rutas").val('create-department')
    });

    $("#table_department tbody").on('click','.edit-department',function (e) {
        e.preventDefault();
        var list_users = $("#table_department").DataTable();
        $('#modal-department').modal('show');
        var rowData = list_users.row( $(this).parents('tr') ).data();
        $.each(rowData, function(index, element) {
            $(".item-"+index).val(element);
            if (index=='id')
            {
                $('.rutas').val('udpate-department/'+element);
            }

        });

    });

    $("#form-save-department").on('submit',function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta =$('.rutas').val();
        method = "POST";
        modal = 'modal-department';
        sendData(data,ruta,method,modal);
        clearSpan();
    });

    $("#table_department tbody").on('click','.changeStatus',function (e) {
       e.preventDefault();
        var data = new FormData();
        data.append('id',$(this).data('id'));
        var ruta = "changeStatus-department";
        var method = "POST";
        var modal = "modal-deparment";
        sendData(data,ruta,method,modal);
    });

    function sendData(data,ruta,method,modal)
    {
        var modalJ = modal;
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $('#'+ modal +'').modal('hide');
                msgSuccess(data.msg,data.success);
                $("#table_department").DataTable().ajax.reload();

            },
            error: function (errors) {
                var indices = errors.responseJSON.errors;
                $.each(indices, function (index, value) {
                    console.log(index,value);
                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });
            }
        });
    }

    function clearSpan() {
        $('#error-name').text('');
    }

});