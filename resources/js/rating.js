$(document).ready(function(){
    // Detecto la estrella que es presionada
    $("li.star a").click(function(event){
        // Recojo el valor de la estrella
        valor_actual=$(this).attr("title");
        // Cambio el estilo para mostrar la estrella seleccionada
        $("li.current-rating").css("width", valor_actual*25);
        // Cambio el valor del campo hidden
        $("#my_vote").attr("value", valor_actual);
    });
});