$(document).ready(function () {
    var ruta,method,data,modal;
   $("#table_branchOffice").DataTable({
      proccesing: true,
      serverSide: true,
      language: {
         "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
      },
      responsive:true,
      ajax:'get-branchOffice-ajax',
      columns:[
         {data: 'id', name:'id'},
         {data: 'name', name:'name'},
      ],
      "columnDefs": [{
         "targets": 2,
         "data": "download_link",
         "render": function ( data, type, row, meta ) {
            var icon , title,colorClas;
            if(row.status != 0 )
            {
               icon = "far fa-check-circle";
               title = "Desactivar";
               colorClas ="primary";
            }else{
               icon = "fas fa-times-circle";
               title = "Activar";
               colorClas = "danger";
            }
            return  '<a href="#" class="btn btn-info edit-branchOffice"    data-val="'+ row +'"    data-toggle="tooltip" title="Editar usuario"><span class="far fa-edit"></span></span></a> '+
                '<a href="#" class="btn btn-'+ colorClas +' changeStatus"   data-id ="'+ row.id +'"  data-toggle="tooltip" title="'+ title +'"><span class="' + icon + '"></span></a> '
         }}
      ]
   });

   $(".new-branch-office").on('click',function (e) {
      e.preventDefault();
      clearSpan();

      $('#modal-branch-office').modal('show');
      $('.rutas').val('new-branch-office');
   });

   $("#form-save-branchOffice").on('submit',function (e) {
      e.preventDefault();
      ruta =$('.rutas').val();
      method= "POST";
      data = new FormData(this);
      modal = "modal-branch-office";

      sendData(data,ruta,method,modal)
   });

   $("#table_branchOffice tbody").on('click',".changeStatus",function (e) {
      e.preventDefault();
      var data = new FormData();
      data.append('id',$(this).data('id'));
      var ruta = "changeStatus-branchOffice";
      var method = "POST";
      var modal = "modal-branch-office";
      sendData(data,ruta,method,modal);
   });

   $("#table_branchOffice tbody").on('click','.edit-branchOffice',function (e) {
      e.preventDefault();
      var list_branch = $("#table_branchOffice").DataTable();
      var rowData = list_branch.row( $(this).parents('tr') ).data();
      $('#modal-branch-office').modal('show');
      $.each(rowData, function(index, element)
      {
         $(".item-" + index).val(element);
         if (index == 'id')
         {
            $('.rutas').val('udpate-branch-office/'+element);
         }

      });
      console.log("ok");
   });

   function sendData(data,ruta,method,modal)
   {
      var modalJ = modal;
      $.ajax({
         url: ruta,
         type: method,
         data: data,
         processData: false,
         contentType: false,
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function (data) {
            console.log(data);

             $('#'+ modal +'').modal('hide');
             msgSuccess(data.msg,data.success);
             $("#table_branchOffice").DataTable().ajax.reload();
         },
         error: function (errors) {
            var indices = errors.responseJSON.errors;
            $.each(indices, function (index, value) {
               console.log(index,value);
               $("#error-" + index).append(value);
               $("#error-" + index).show();
            });
         }
      });
   }

   function clearSpan() {

      $('#error-name').text('');
      $('.item-name').html('');
      $('.item-name').val('');
   }

});