$(document).ready(function () {
    window.msgSuccess = function(msg,type)
    {
        Swal.fire({
            position: 'center',
            icon: type,
            title: msg,
            showConfirmButton: false,
            timer: 1500
        })
    }

});
