$(document).ready(function () {
    $("#table_categories").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive:true,
        ajax:'get-categories-ajax',
        columns:[
            {data: 'id', name:'id'},
            {data: 'name', name:'name'},
            {data: 'description', name:'description'},

        ],
        "columnDefs": [{
            "targets": 3,
            "data": "download_link",
            "render": function ( data, type, row, meta ) {
                return  '<a href="#" class="btn btn-info edit-category"    data-val="'+ row +'"    data-toggle="tooltip" title="Editar categoria"><span class="far fa-edit"></span></span></a> '
            }}
        ]
    });

    $(".create-categorie").on('click',function (e) {
        e.preventDefault();
        $(".titleModal").html("Nueva categoria");
        $('.ruta').val("save-category");
        $("#modal-category").modal('show');
    });

    $("#form-save-category").on('submit',function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta =$('.ruta').val();
        method = "POST";
        modal = 'modal-category';
        sendData(data,ruta,method,modal);
        clearSpan();
    });

    $("#table_categories tbody").on('click','.edit-category',function (e) {
        e.preventDefault();
        var list_categories = $("#table_categories").DataTable();
        clearSpan();
        $(".titleModal").text('Editar categoria');
        $('#modal-category').modal('show');
        var rowData = list_categories.row( $(this).parents('tr') ).data();
        $.each(rowData, function(index, element) {
            console.log(index,element)
                $("#item-" + index).val(element);
                if (index == 'id') {
                    $('.ruta').val('udpate-category/' + element);
                }
            }
        )
    });

    function sendData(data,ruta,method,modal)
    {
        var modalJ = modal;
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data);
                $('#'+ modal +'').modal('hide');
                msgSuccess(data.msg,data.success);
                $("#table_categories").DataTable().ajax.reload();

            },
            error: function (errors,xhr, status,) {
                console.log(status);
                var msg = JSON.parse(errors.responseText);
                if (status == "Forbidden")
                {
                    msgSuccess(msg.message,"error");
                    return true;
                }


                var indices = errors.responseJSON.errors;
                $.each(indices, function (index, value) {
                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });
            }
        });
    }

    function clearSpan() {
        $('#error-name').text('');
        $('#error-descripcion').text('');
    }


});