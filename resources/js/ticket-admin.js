$("document").ready(function () {
   var sucursal = $("#usuario_id");

   $(".oppen-form-new-ticket").on('click',function (e) {
      e.preventDefault();
      $("#modal-ticket-admin").modal('show');
   });

   $(".select-sucursal").change(function(){
      var data = new FormData();
      data.append('sucursal_id',$('select[id=sucursal_id]').val());
      ruta = "get-branchOffices";
      method = "POST";
      sendData(data,ruta,method)

   });


   function sendData(data,ruta,method)
   {
      $.ajax({
         url: ruta,
         type: method,
         data: data,
         processData: false,
         contentType: false,
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function (data) {
            sucursal.find('option').remove();

            $(data).each(function(i, v){ // indice, valor
               sucursal.append('<option value="' + v.id + '">' + v.name + '</option>');
            })

            sucursal.prop('disabled', false);
         },
         error: function (errors,xhr, status,) {
            var indices = errors.responseJSON.errors;
            $.each(indices, function (index, value) {

               $("#error-" + index).append(value);
               $("#error-" + index).show();
            });
         }
      });
   }

   $("#form-save-ticket-admin").on('submit',function (e) {
      e.preventDefault();

      $.ajax({
         url: "save-ticket-by-admin",
         type: "post",
         data: new FormData(this),
         processData: false,
         contentType: false,
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function (data) {
            msgSuccess(data.msg,data.success);
            $('#table_list_tickets').DataTable().ajax.reload();
            $("#modal-ticket-admin").modal('hide');
         },
         error: function (errors,xhr, status,) {
            var indices = errors.responseJSON.errors;
            $.each(indices, function (index, value) {

               $("#error-" + index).append(value);
               $("#error-" + index).show();
            });
         }
      });
   })

});