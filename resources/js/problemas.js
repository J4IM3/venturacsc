$(document).ready(function () {
    $("#problems_table").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive:true,
        ajax:'get-users-problems',
        columns:[
            {data: 'id', name:'id'},
            {data: 'problema', name:'problema'},
        ],
        "columnDefs": [{
            "targets": 2,
            "data": "download_link",
            "render": function ( data, type, row, meta ) {

                var icon , title,colorClas;
                if(row.status != 0 )
                {
                    icon = "far fa-check-circle";
                    title = "Desactivar";
                    colorClas ="primary";
                }else{
                    icon = "fas fa-times-circle";
                    title = "Activar";
                    colorClas = "danger";
                }

                return  '<a href="#" class="btn btn-info edit-problem"    data-val="'+ row +'"    data-toggle="tooltip" title="Editar usuario"><span class="far fa-edit"></span></span></a> '+
                    '<a href="#" class="btn btn-'+ colorClas +' changeStatus-problem"   data-id ="'+ row.id +'"  data-toggle="tooltip" title="'+ title +'"><span class="' + icon + '"></span></a> '
            }}
        ]
    });

    $(".problama-nuevo").on('click',function (e) {
        e.preventDefault();
        $("#modal-create-problem").modal("show");
        $('.rutas').val('create-problem');
        $("#item-problema").val('');
    })

    $("#form-save-problem").on('submit',function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta =$('.rutas').val();
        method = "POST";
        modal = 'modal-create-problem';
        sendData(data,ruta,method,modal);
        clearSpan();
    });

    $("#problems_table tbody").on('click','.edit-problem',function (e) {
        e.preventDefault();
        var list_problems = $("#problems_table").DataTable();
        $('#modal-create-problem').modal('show');
        var rowData = list_problems.row( $(this).parents('tr') ).data();
        $.each(rowData, function(index, element) {
            if (index == "id")
            {
                $('.rutas').val('udpate-problem/'+element);
                $('.id').val(element);
            }

            console.log(index,element);
            $("#item-"+index).val(element);
        });

    });

    $("#problems_table tbody").on('click','.changeStatus-problem',function (e) {
        e.preventDefault();
        var data = new FormData();
        data.append('id',$(this).data('id'));
        var ruta = "changeStatus-problem";
        var method = "POST";
        var modal = "modal-create-problem";
        sendData(data,ruta,method,modal);
    });


    function sendData(data,ruta,method,modal)
    {
        var modalJ = modal;
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data);
                $('#'+ modal +'').modal('hide');
                msgSuccess(data.msg,data.success);
                $("#problems_table").DataTable().ajax.reload();

            },
            error: function (errors,xhr, status,) {

                var msg = JSON.parse(errors.responseText);
                if (status == "Forbidden")
                {
                    msgSuccess(msg.message,"error");
                    return true;
                }


                var indices = errors.responseJSON.errors;
                $.each(indices, function (index, value) {

                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });
            }
        });
    }

    function clearSpan() {
        $('#error-problema').text('');
    }


});