$(document).ready(function () {
    $(".divLoading").hide();
    var tb = " ";

    $("#table_tickets").DataTable({
        proccesing: true,
        serverSide: true,
        order: [ [0, 'desc'] ],
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive:true,
        ajax:'get-tickets-ajax',
        columns:[
            {data: 'id', name:'id'},
            {data: 'tickets_problemas.problema', name:'tickets_problemas.problema'},
            {data: 'asignado', name:'asignado'},
            {data: 'status', name:'status'},
        ],
        "columnDefs": [{
            "targets": 4,
            "data": "download_link",
            "render": function ( data, type, row, meta ) {
                console.log(row);
                var icon , title,colorClas;
                if(row.status === "Finalizado" )
                {
                    var finalizado = "";
                }else{
                    var finalizado = '<a href="#" class="btn btn-success finalizarTicket"   data-slug ="'+ row.slug+'"  data-toggle="tooltip" title="Finalizar ticket"><i class="fas fa-flag-checkered"></i></a> '
                }
                return'<a href="'+ '/view-ticket/' + row.slug+ '" class="btn btn-info" data-toggle="tooltip" title="Ver detalle del ticket"><i class="fas fa-layer-group"></i></a> '+
                    '<a href="#" class="btn btn-primary addFiles"   data-slug ="'+ row.slug+'"  data-toggle="tooltip" title="Agregar archivo a ticket"><i class="far fa-plus-square"></i></a> '+ finalizado


            }}
        ]
    });


    $("#table_list_tickets").DataTable({
        proccesing: true,
        serverSide: true,
        order: [ [0, 'desc'] ],
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive:true,
        ajax:'get-list_tickets-ajax',
        columns:[
            {data: 'id', name:'id'},
            {data: 'users_ticket.user_sucursal.name', name:'users_ticket.user_sucursal.name'},
            {data: 'users_ticket.name', name:'users_ticket.name'},
            {data: 'tickets_problemas.problema', name:'tickets_problemas.problema'},
            {data: 'status', name:'status'},
            {data: 'asignado', name:'asignado'},
        ],
        "columnDefs": [{
            "targets": 6,
            "data": "download_link",
            "render": function ( data, type, row, meta ) {
                return'<a href="'+ '/view-ticket-admin/' + row.slug+ '" class="btn btn-info" data-toggle="tooltip" title="Ver ticket"><i class="fas fa-layer-group"></i></a> '+
                    '<a href="#" class="btn btn-primary addFilesAdmin"   data-slug ="'+ row.slug+'"  data-toggle="tooltip" title="Agregar archivo a ticket"><i class="far fa-plus-square"></i></a> '+
                    '<a href="#" class="btn btn-primary pending_ticket" data-id ="'+ row.slug +'"  data-toggle="tooltip" title="Colocar ticket en espera "><i class="far fa-clock"></i></span></a> '+
                    '<a href="#" class="btn btn-primary assign_ticket " data-id ="'+ row.slug +'"  data-toggle="tooltip" title="Asignar ticket"><i class="far fa-hand-pointer"></i></span></a> '
            }}
        ]
    });

 $(".new-ticket").on('click',function (e) {
     e.preventDefault();
     $(".titleModal").html("Nuevo ticket");
     $("#btnFetch").prop("disabled", false);
     // add spinner to button
     $("#btnFetch").html(
         '<span></span> Guardar'
     );
     $("#modal-create-ticket").modal('show');
 });

 $('#table_list_tickets tbody').on( 'click', '.pending_ticket', function (e) {
     e.preventDefault();
     var data_tickets = $("#table_list_tickets").DataTable();
     var rowData = data_tickets.row( $(this).parents('tr') ).data();
     $.each(rowData, function(index, element) {
         if (index == "slug") {
             $(".slug-ticket").val(element);
         }
     });
     $('#modal-on-hold').modal('show');

 });

    $("#form-save-ticket-pending").on('submit',function (e) {
        e.preventDefault();
        var data = new FormData(this);
        var ruta= "save-ticket-pending";
        var method = "POST";
        var modal = 'modal-on-hold';
        tb = "#table_list_tickets";
        clearSpan();
        sendData(data,ruta,method,modal,tb);
    });



    $("#form-save-ticket").on('submit',function (e) {
     e.preventDefault();
     var data = new FormData(this);
     var ruta= "save-ticket";
     var method = "POST";
     var modal = 'modal-create-ticket';
     tb = "#table_tickets";
     clearSpan();
     sendData(data,ruta,method,modal,tb);
     $("#btnFetch").prop("disabled", true);
     // add spinner to button
     $("#btnFetch").html(
         `<span class="spinner-grow spinner-grow-sm " role="status" aria-hidden="true"></span> Espere un momento...`
     );
 });

 $("#table_list_tickets tbody").on('click','.assign_ticket',function (e) {
     e.preventDefault();
     var data_tickets = $("#table_list_tickets").DataTable();
     var rowData = data_tickets.row( $(this).parents('tr') ).data();
     $.each(rowData, function(index, element) {
         if (index == "id") {
             $(".num_ticket_value").val(element);
         }
     });
     $("#modal-assign").modal('show');
 });

 $("#save-assign-user").on('submit',function (e) {
     e.preventDefault();
     $("#modal-assign").modal('hide');
     var data = new FormData(this);
     var ruta= "save-user-assigned";
     var method = "POST";
     var modal = 'modal-assign';
     tb = "#table_list_tickets";
     clearSpan();
     sendData(data,ruta,method,modal,tb);
 });

    /**
     * Finalizar ticket
     * @param data
     * @param ruta
     * @param method
     * @param modal
     */

    $("#table_tickets tbody").on('click','.finalizarTicket',function (e) {
        e.preventDefault();
        $("#modal-finish-submit")[0].reset();

        var slug = $(this).data('slug');
        $(".slug-calif").val(slug)
        $("#modal-calif").modal('show');
        /*
        var data = new FormData();
        data.append('slug',$(this).data('slug'));
        var ruta= "save-ticket-finalize";
        var method = "POST";
        var modal = 'modal-calif';
        tb="#table_tickets";
        clearSpan();
        calif(data,ruta,method,modal,tb);
         */
    });

    $("#modal-finish-submit").on('submit',function (e) {
        e.preventDefault();
        var data = new FormData(this);
        var ruta= "save-calif-ajax";
        var method = "POST";
        var modal = 'modal-calif';
        tb = "#table_tickets";
        sendData(data,ruta,method, modal,tb)
    });

    $("#table_tickets tbody").on('click','.addFiles',function () {
        var slug = $(this).data('slug');
        $(".slug-files").val(slug)
        $("#modal-add-files").modal('show');
        $(".campos2").empty();
    });

    $("#table_list_tickets tbody").on('click','.addFilesAdmin',function () {
        var slug = $(this).data('slug');
        $(".slug-files").val(slug)
        $(".modal-add-files-admin").modal('show');
        $(".campos2").empty();
    });

    $("#form-add-files-ticket").on('submit',function (e) {
        e.preventDefault();
        var data = new FormData(this);
        var ruta= "save-add-files";
        var method = "POST";
        var modal = 'modal-add-files';
        tb = "table_list_tickets";
        sendData(data,ruta,method, modal,tb)

    });

    function sendData(data,ruta,method,modal,tb)
    {
        var modalJ = modal;
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data);
                $('#'+ modal +'').modal('hide');
                msgSuccess(data.msg,data.success);
                $(''+ tb + '').DataTable().ajax.reload();

            },
            error: function (errors,xhr, status,) {
                $("#btnFetch").prop("disabled", false);
                // add spinner to button
                $("#btnFetch").html(
                    '<span></span> Guardar'
                );

                var msg = JSON.parse(errors.responseText);
                if (status == "Forbidden")
                {
                    msgSuccess(msg.message,"error");
                    return true;
                }


                var indices = errors.responseJSON.errors;
                $.each(indices, function (index, value) {

                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });
            }
        });
    }

    function clearSpan()
    {
        $('#error-problema').text('');
        $('#error-descripcion').text('');
        $('#error-branchOffice_id').text('');
        $('#error-telefono').text('');
        $('#error-contacto').text('');
        $('#error-hora_inicio').text('');
        $('#error-hora_fin').text('');
        $('#error-teamviewer_id').text('');
        $('#error-teamviewer_password').text('');
    }

    $(".agregarCampos").on('click',function (e) {
        e.preventDefault();
        var nextinput = 0;
        nextinput++;
        campo = '<li id="rut'+nextinput+'"><input  class="form-control" type="file" size="20" id="files[]" name="files[]"/></li>';
        $("#campos").append(campo);
    });
    $(".agregarCampos2").on('click',function (e) {
        e.preventDefault();
        var nextinput = 0;
        nextinput++;
        campo = '<li id="rut'+nextinput+'"><input required class="form-control addfiles[]" style="list-style: none;" type="file" size="20"  name="addfiles[]"/></li>';
        $(".campos2").append(campo);
    });

    $("#btnFetch").click(function() {
    });
});