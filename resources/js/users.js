$(document).ready(function () {
    $("#table_users").DataTable({
        proccesing: true,
        serverSide: true,
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        responsive:true,
        ajax:'get-users-ajax',
        columns:[
            {data: 'id', name:'id'},
            {data: 'name', name:'name'},
            {data: 'email', name:'email'},
        ],
        "columnDefs": [{
            "targets": 3,
            "data": "download_link",
            "render": function ( data, type, row, meta ) {

                var icon , title,colorClas;
                if(row.status != 0 )
                {
                    icon = "far fa-check-circle";
                    title = "Desactivar";
                    colorClas ="primary";
                }else{
                    icon = "fas fa-times-circle";
                    title = "Activar";
                    colorClas = "danger";
                }
                /*
                if(row.type == "admin")
                {
                    var tipo = '<a href="#" class="btn btn-'+ colorClas +' changeStatus"   data-id ="'+ row.id +'"  data-toggle="tooltip" title="'+ title +'"><span class="' + icon + '"></span></a> '
                }else{
                    var tipo = "";
                }
                */
                return  '<a href="#" class="btn btn-info edit-user"    data-val="'+ row +'"    data-toggle="tooltip" title="Editar usuario"><span class="far fa-edit"></span></span></a> '+
                        '<a href="#" class="btn btn-'+ colorClas +' changeStatus"   data-id ="'+ row.id +'"  data-toggle="tooltip" title="'+ title +'"><span class="' + icon + '"></span></a> '
            }}
        ]
    });

    $(".new-user").on('click',function (e) {
        e.preventDefault();
        $(".section-password").removeClass("d-none");
        $('.section-password').find('input').prop('disabled',false);
        clearSpan();
        $('#form-save-user').trigger("reset");
        $(".titleModal").text('Nuevo usuario');

        $('#modal-users').modal('show');
        $('.rutas').val('create');

    });

    $("#form-save-user").on("submit", function (e) {
        e.preventDefault();
        var data = new FormData(this);
        ruta =$('.rutas').val();
        method = "POST";
        modal = 'modal-users';
        sendData(data,ruta,method,modal);
        clearSpan();
    });

    $('#table_users tbody').on( 'click', '.edit-user', function (e) {
        var list_users = $("#table_users").DataTable();
        e.preventDefault();
        clearSpan();
        $(".titleModal").text('Editar usuario');$(".section-password").addClass("d-none");
        $('.section-password').find('input').attr('disabled','disabled');
        $('#modal-users').modal('show');
        var rowData = list_users.row( $(this).parents('tr') ).data();
        $.each(rowData, function(index, element) {

            $(".item-"+index).val(element);
            if (index=='id')
            {
                $('.rutas').val('udpate/'+element);
            }
            if (index == 'department_id' || index == 'branchOffice_id' || index == "type") {
                console.log(index,element);
                $(".item-"+index).val(element).trigger("chosen:updated");
                $(".item-"+index).val(element).trigger("chosen:updated");
                $("#item-"+index).val(element).trigger("chosen:updated");
                //$("#item-"+ index +"option:selected" ).text(element);
            }
        });
    });

    /**
     * Cambiar el status de un usuario
     */

    $("#table_users tbody").on('click','.changeStatus',function (e) {
        e.preventDefault();
        var data = new FormData();
        data.append('id',$(this).data('id'));
        var ruta = "changeStatus";
        var method = "POST";
        var modal = "modal-users";
        sendData(data,ruta,method,modal);
    });

    /**
     * Mostrar modal para cambiar contraseña
     */
    $(".update-password").on('click',function (e) {
        e.preventDefault();
        $("#change-password").modal('show');
    });

    $("#form-change-password").on("submit",function (e) {
        e.preventDefault();
        var data = new FormData(this);
        var ruta= "update-password";
        var method = "POST";
        var modal = 'change-password';
        clearSpan();
        sendData(data,ruta,method,modal)
    });

    function sendData(data,ruta,method,modal)
    {
        var modalJ = modal;
        $.ajax({
            url: ruta,
            type: method,
            data: data,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {

                    $('#'+ modal +'').modal('hide');
                    msgSuccess(data.msg,data.success);
                    $("#table_users").DataTable().ajax.reload();

            },
            error: function (errors,xhr, status,) {

                var msg = JSON.parse(errors.responseText);
                if (status == "Forbidden")
                {
                    msgSuccess(msg.message,"error");
                    return true;
                }


                var indices = errors.responseJSON.errors;
                $.each(indices, function (index, value) {

                    $("#error-" + index).append(value);
                    $("#error-" + index).show();
                });
            }
        });
    }

    $(".add_deppartent").on('click',function (e) {
        e.preventDefault();
        console.log("Ok");
        $('#modal-users').modal('hide');
        $('#modal-addDepartment').modal('show');

    });

    $(".send-data-department").on('click',function (e) {
        e.preventDefault();
        $('#modal-users').modal('show');
        var depa = $(".item-department-add").val();
        $('#modal-addDepartment').modal('hide');

        $('#item-department_id').append($('<option>', {
            value: depa ,
            text: depa
        }));

    });
    function clearSpan() {
        $('#error-name').text('');
        $('#error-email').text('');
        $('#error-password').text('');
        $('#error-password_confirmation').text('');
    }

});