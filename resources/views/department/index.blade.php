@extends('layouts.app')
@section('content')
    <div class="container">
        <h2>Listado departamentos</h2>
        <br><br>
        <button class="btn btn-primary new-department">Nuevo departamento</button>
        <br><br>
        <table id="table_department">
        </table>
        @include('department.modal-new-department')
    </div>
    @endsection