@extends('layouts.app')
@section('content')
    <div class="container ">
        <div class="row">

            <div class="col-md-4">
                <div class="card text-white bg-success mb-3" style="max-width: 20rem;">
                    <div class="card-header bg-light" style="color: grey" align="center"> <h3> Tickets finalizados</h3>  </div>
                    <div class="card-body">
                        <p class="card-text">
                            <h1><i class="far fa-check-circle"></i></h1>
                        <h1> {{ $ticketsFinalizadosPorUsuario }}</h1>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card text-white bg-warning mb-3" style="max-width: 20rem;">
                    <div class="card-header bg-light" style="color: grey" align="center"> <h3> Tickets pendientes</h3>  </div>
                    <div class="card-body">
                        <p class="card-text">
                        <h1><i class="far fa-hourglass"></i></h1>
                        <h1>{{ $ticketsPendientesPorUsuario }}</h1>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card text-white bg-primary mb-3" style="max-width: 20rem;">
                    <div class="card-header bg-light" style="color: grey" align="center"> <h3> Total tickets </h3>  </div>
                    <div class="card-body">
                        <p class="card-text">
                        <h1><i class="fas fa-chart-line"></i></h1>
                        <h1>{{ $totalTickets }}</h1>
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @endsection