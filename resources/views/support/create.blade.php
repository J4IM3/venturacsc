<div class="modal fade" id="modal-create-ticket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><p class="titleModal"></p></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-save-ticket" class="form-add-user" autocomplete="off" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group form-row">
                     @include('support.fields')
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" id="btnFetch" class="btn btn-primary send-data-user">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>