@extends('layouts.app')
@section('content')
    <div class="container">

            <form action="{{ route('envio-respuesta') }}" method="POST">
                @csrf
                <div class="col-md-12 offset-md-3">

                    <p>¿ Que tan satisfecho estas con la atención ? </p>
                    <p class="clasificacion">
                        <input id="radio1" type="radio" name="satisfecho_atencion"  value="5">
                        <label for="radio1" class="start">★</label>
                        <input id="radio2" type="radio" name="satisfecho_atencion"  value="4">
                        <label for="radio2" class="start">★</label>
                        <input id="radio3" type="radio" name="satisfecho_atencion"  value="3">
                        <label for="radio3" class="start">★</label>
                        <input id="radio4" type="radio" name="satisfecho_atencion"  value="2">
                        <label for="radio4" class="start">★</label>
                        <input id="radio5" type="radio" name="satisfecho_atencion"  value="1">
                        <label for="radio5" class="start">★</label>
                    </p>
                </div>
                <br><br>
                <div class="col-md-12 offset-md-3">
                    <div class="box">
                            ¿ Que tan rápido fue la solución ? <br>
                        <div class="ratin_ter">
                            <input type="radio" class="radio_input" name="rapidez_solucion" value="10" id="rate1"> <label for="rate1">10</label>
                            <input type="radio" class="radio_input" name="rapidez_solucion" value="9" id="rate2"> <label for="rate2">9</label>
                            <input type="radio" class="radio_input" name="rapidez_solucion" value="8" id="rate3"> <label for="rate3">8</label>
                            <input type="radio" class="radio_input" name="rapidez_solucion" value="7" id="rate4"> <label for="rate4">7</label>
                            <input type="radio" class="radio_input" name="rapidez_solucion" value="6" id="rate5"> <label for="rate5">6</label>
                            <input type="radio" class="radio_input" name="rapidez_solucion" value="5" id="rate6"> <label for="rate6">5</label>
                            <input type="radio" class="radio_input" name="rapidez_solucion" value="4" id="rate7"> <label for="rate7">4</label>
                            <input type="radio" class="radio_input" name="rapidez_solucion" value="3" id="rate8"> <label for="rate8">3</label>
                            <input type="radio" class="radio_input" name="rapidez_solucion" value="2" id="rate9"> <label for="rate9">2</label>
                            <input type="radio" class="radio_input" name="rapidez_solucion" value="1" id="rate10"> <label for="rate10">1</label>
                        </div>

                    </div>
                </div>
                <br><br>
                <div class="col-md-12 offset-md-3">

                    <div class="box">
                        ¿ Como calificas nuestro soporte ? <br>
                        <div class="ratin_ter2">
                            <input type="radio" class="radio_input" name="atencion_soporte" value="10" id="rate11"> <label for="rate11">10</label>
                            <input type="radio" class="radio_input" name="atencion_soporte" value="9" id="rate12"> <label for="rate12">9</label>
                            <input type="radio" class="radio_input" name="atencion_soporte" value="8" id="rate13"> <label for="rate13">8</label>
                            <input type="radio" class="radio_input" name="atencion_soporte" value="7" id="rate14"> <label for="rate14">7</label>
                            <input type="radio" class="radio_input" name="atencion_soporte" value="6" id="rate15"> <label for="rate15">6</label>
                            <input type="radio" class="radio_input" name="atencion_soporte" value="5" id="rate16"> <label for="rate16">5</label>
                            <input type="radio" class="radio_input" name="atencion_soporte" value="4" id="rate17"> <label for="rate17">4</label>
                            <input type="radio" class="radio_input" name="atencion_soporte" value="3" id="rate18"> <label for="rate18">3</label>
                            <input type="radio" class="radio_input" name="atencion_soporte" value="2" id="rate19"> <label for="rate19">2</label>
                            <input type="radio" class="radio_input" name="atencion_soporte" value="1" id="rate110"> <label for="rate110">1</label>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="slug" value="{{ $slug }}">
                <br><br>
                <div class="col-md-9" >
                    <button type="submit" class="btn btn-primary float-right">Calificar servicio</button>
                </div>
            </form>
    </div>
    @endsection()