@extends('layouts.app')
@section('content')
    <div class="container">
        <h2>Listado de tickets</h2>
        <br>
        <button class="btn btn-primary oppen-form-new-ticket">Crear ticket  </button>
        <br>
        <br>
        @include('support.modal-on-hold')
        @include('support.moda-assign-ticket')
        @include('support.modal-add-files')
        @include('support.modal-create-ticket-admin')
        <table id="table_list_tickets">
            <thead>
            <tr>
                <th>Id</th>
                <th>Sucursal</th>
                <th>Usuario</th>
                <th>Problema</th>
                <th>Estatus</th>
                <th>Asignado</th>
                <th>Opciones</th>
            </tr>
            </thead>
        </table>
    </div>
    @endsection