<div class="modal fade" id="modal-ticket-admin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><p class="titleModal"></p></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="form-save-ticket-admin" class="form-add-user" autocomplete="off">
                <div class="modal-body">

                    <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fas fa-building"></i></span>
                            </div>

                            <select name="branchOffice_id" id="sucursal_id" class="form-control select-sucursal " style="width: 50%" required>
                                <option value="">Selecionar sucursal</option>
                                @foreach($sucursales as $sucursal)
                                    <option value="{{ $sucursal->id }}">{{ $sucursal->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span class="badge badge-danger" id="error-problema_id"></span>
                        </div>
                    </div>


                    <div class="col-md-6 col-sm-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fas fa-users"></i></span>
                            </div>
                                <select id="usuario_id"  name="usuario_id" class="form-control">
                                <option value="">Selecciona un usuario</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span class="badge badge-danger" id="error-problema_id"></span>
                        </div>
                    </div>

                        <div class="col-md-4">
                            <div class="input-group mb-3 ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                                </div>
                                <input name="contacto"  class="form-control item-contacto" placeholder="Ingresar persona p/ contacto" aria-label="Nombre" aria-describedby="basic-addon1" id="exampleFormControlTextarea1" >
                            </div>

                            <div class="col-md-4">
                                <span class="badge badge-danger" id="error-contacto"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="input-group mb-3 ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-phone-square-alt"></i></span>
                                </div>
                                <input name="telefono"  class="form-control item-telefono" placeholder="Tel contacto" aria-label="Nombre" aria-describedby="basic-addon1" id="exampleFormControlTextarea1" >
                            </div>

                            <div class="col-md-4">
                                <span class="badge badge-danger" id="error-telefono"></span>
                            </div>
                        </div>

                    <div class="col-md-4">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fas fa-exclamation-circle"></i></span>
                            </div>

                            <select name="problema_id" id="item-problema_id" class="form-control " style="width: 50%" required>
                                <option value="">Selecionar problema</option>
                                @foreach($problemas as $problema)

                                    <option value="{{ $problema->id }}">{{ $problema->problema}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12">
                            <span class="badge badge-danger" id="error-problema_id"></span>
                        </div>
                    </div>



                        <div class="col-md-12">
                            <div class="input-group mb-3 ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-exclamation-circle"></i></span>
                                </div>
                                <textarea name="descripcion"  class="form-control item-descripcion" placeholder="Ingresar descripcion ejem: La red dejo de funcionar cuando .."  aria-label="Nombre" aria-describedby="basic-addon1" id="exampleFormControlTextarea1" rows="2"></textarea>
                            </div>
                            <div class="col-md-12">
                                <span class="badge badge-danger" id="error-descripcion"></span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"> <i class="fas fa-server"></i> </span>
                                </div>
                                <input type="text" name="teamviewer_id"  class="form-control item-teamviewer_id" placeholder="Id teamviewer"  aria-label="Nombre" aria-describedby="basic-addon1" id="exampleFormControlTextarea1" >
                            </div>

                            <div class="col-md-12">
                                <span class="badge badge-danger" id="error-teamviewer_id"></span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"> <i class="fas fa-unlock-alt"></i> </span>
                                </div>
                                <input type="text" name="teamviewer_password"  class="form-control item-teamviewer_password" placeholder="Contraseña teamviewer"  aria-label="Nombre" aria-describedby="basic-addon1" id="exampleFormControlTextarea1" >
                            </div>

                            <div class="col-md-12">
                                <span class="badge badge-danger" id="error-teamviewer_password"></span>
                            </div>
                        </div>



                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary send-data-user">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>