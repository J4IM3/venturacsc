<div class="modal fade" id="modal-on-hold" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><p class="titleModal"></p></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="form-save-ticket-pending" class="form-add-user" autocomplete="off">
                <div class="modal-body">

                    <div class="col-md-12">
                        <div class="input-group mb-3 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="date" name="fecha"  class="form-control item-fecha" placeholder="Ingresar fecha" aria-label="Nombre" aria-describedby="basic-addon1" >
                        </div>

                        <div class="col-md-4">
                            <span class="badge badge-danger" id="error-contacto"></span>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="input-group mb-3 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="far fa-comments"></i></span>
                            </div>
                            <input name="motivo"  class="form-control item-motivo" placeholder="Ingresar motivo" aria-label="Nombre" aria-describedby="basic-addon1" >
                        </div>

                        <div class="col-md-4">
                            <span class="badge badge-danger" id="error-contacto"></span>
                        </div>
                    </div>
                    <input type="hidden" name="slug" value="" class="form-control slug-ticket">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary send-data-user">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>