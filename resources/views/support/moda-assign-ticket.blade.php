<div class="modal" tabindex="-1" role="dialog" id="modal-assign">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Asignar ticket</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="save-assign-user">
                <div class="modal-body">

                    <select name="user_assigned" id="" class="form-control">
                        @foreach($usersAdmin as $admin)
                            <option value="{{ $admin->id }}"> {{ $admin->name }}</option>
                            @endforeach
                    </select>
                    <input type="hidden" value="" name="num_ticket" class="num_ticket_value form-control">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Asignar</button>
                </div>
            </form>
        </div>
    </div>
</div>