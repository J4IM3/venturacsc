<div class="modal fade modal-add-files-admin" id="modal-add-files" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><p >Agregar archivos a ticket</p></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="form-add-files-ticket" class="form-add-user" autocomplete="off">
                <div class="modal-body">
                    <div class="col-md-12">
                        <button class="btn btn-success agregarCampos2">Agregar archivo</button>
                        <div class="campos2"></div>
                    </div>
                    <input type="hidden" name="slug" value="" class="form-control slug-files">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary send-data-user">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>