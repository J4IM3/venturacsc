@extends('layouts.app')
@section('content')
        <div class="container">
        <h2>Detalle ticket </h2>
        <br>
            ** Fecha: {{ $ticket[0]->fecha }}
            @if(!empty($adminSupport->history_assignations->name))

            **Asignado a ** {{ $adminSupport->history_assignations->name }}
            @endif
            <table class="table table-bordered tab-content table-hover">
                <tbody>
                    <tr>
                        <td>Solicitante: {{ $ticket[0]->usersTicket->name }}</td>

                        <td>Sucursal: {{ $ticket[0]->sucursalTicket->name }} </td>
                        <td>Departamento: {{ $ticket[0]->department_ticket->name }}</td>
                        <td>Id teamviewer: {{ $ticket[0]->teamviewer_id }} </td>
                    </tr>
                    <tr>
                        <td>Persona p/ contacto: {{ $ticket[0]->contacto }}</td>
                        <td>Teléfono contacto: {{ $ticket[0]->telefono }}</td>
                        <td>Horario contacto: {{ $ticket[0]->horario_contacto }}</td>
                        <td>Contraseña teamviewer: {{ $ticket[0]->teamviewer_password }} </td>
                    </tr>
                    <tr>
                        <td>
                            Problema: {{ $ticket[0]->ticketsProblemas->problema}}
                        </td>
                        <td colspan="3">
                            Descripción: {{ $ticket[0]->descripcion}}
                        </td>
                    </tr>
                </tbody>
            </table>

            @if(!empty( $ticket[0]->descripcion))

            @endif

            @if(!empty($ticket[0]->ticketFile ))

            <table>
                <thead>
                <th>Archivo</th>
                </thead>
                <tbody>
                @foreach($ticket[0]->ticketFile as $file)
                    <tr>
                        <td>
                            <a href="{{ asset('files/'.$file->name) }}"  target="_blank">{{ $file->name }}</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endif


        <hr class="line">
        <div class="container-comments col-md-12" style="width: 120%">
            @if(!empty($comments))
            @foreach($comments as $comment)
            <div class="comments">

                <div class="photo-perfil">
                <img src="{{ $comment->usersComments->imagen}}/{{ $comment->usersComments->imagen_name }} " class="" alt="">
                </div>



                <div class="info-comments">
                    <div class="header">
                        <h4>{{ $comment->usersComments->name }}</h4>
                        <h5>{{ $comment->fecha }}</h5>
                    </div>
                    <p>{{ $comment->comment}}</p>
                </div>
            </div>
            @endforeach
                @endif
        </div>

            <br>

            <form action="{{ route('form-sent-comment') }}" autocomplete="off" enctype="multipart/form-data" method="POST">
                <div class="col-md-12">
                    <div class="input-group mb-3 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-exclamation-circle"></i></span>
                        </div>
                        <textarea name="comment"  required class="form-control item-comment" placeholder="Ingresar respuesta"  aria-label="Nombre" aria-describedby="basic-addon1" id="exampleFormControlTextarea1" rows="4"></textarea>
                    </div>
                    <div class="col-md-12">
                        <span class="badge badge-danger" id="error-comment"></span>
                    </div>
                    @if(!empty($adminSupport->assigned_user ))
                        @if($adminSupport->assigned_user == Auth::id() )
                        <div class="form-check">
                            <input type="checkbox" name="solucion" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Marcar este comentario como solución</label>
                        </div>
                        @endif
                    @endif

                </div>
                <input type="hidden" name="slug"  class="form-control " value="{{ $ticket[0]->slug}}" >
                <input type="hidden" name="user_id"  class="form-control " value="{{ Auth::user()->id}}" >
                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>

                <div class="float-right">
                    <a href="{{ URL::previous() }}" class="btn btn-primary">Regresar</a>
                    <button type="submit" id="btnFetch" class="btn btn-primary send-data-user ">Enviar comentario</button>
                </div>

            </form>



    </div>
    @endsection

