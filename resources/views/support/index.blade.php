 @extends('layouts.app')
 @section('content')
     <div class="container">
         <h3>Mis tickets</h3>
         <button class="btn btn-primary new-ticket">Nuevo ticket</button>
         <br> <br>
         @include('support.create')
         @include('support.modal-finish')
         @include('support.modal-add-files')
         <table id="table_tickets">
             <thead>
             <th>Id</th>
             <th>Problema</th>
             <th>Asignado</th>
             <th>Estatus</th>
             <th>Acciones</th>
             </thead>
         </table>
     </div>
     @endsection