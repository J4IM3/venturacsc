
<div class="col-md-12 row">

    <div class="col-md-12">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-exclamation-circle"></i></span>
            </div>

            <select name="problema_id" id="item-problema_id" class="form-control " style="width: 50%" required>
                <option value="">Selecionar problema</option>
                @foreach($problemas as $problema)

                    <option value="{{ $problema->id }}">{{ $problema->problema}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-12">
            <span class="badge badge-danger" id="error-problema_id"></span>
        </div>
    </div>

    <div class="col-md-12">
        <div class="input-group mb-3 ">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-exclamation-circle"></i></span>
            </div>
            <textarea name="descripcion"  class="form-control item-descripcion" placeholder="Ingresar descripcion ejem: La red dejo de funcionar cuando .."  aria-label="Nombre" aria-describedby="basic-addon1" id="exampleFormControlTextarea1" rows="2"></textarea>
        </div>
        <div class="col-md-12">
            <span class="badge badge-danger" id="error-descripcion"></span>
        </div>
    </div>

    <div class="col-md-2">
        <div class="input-group mb-3 ">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-phone-square-alt"></i></span>
            </div>
            <input name="telefono"  class="form-control item-telefono" placeholder="Tel contacto" required aria-label="Nombre" aria-describedby="basic-addon1" id="exampleFormControlTextarea1" >
        </div>

        <div class="col-md-4">
            <span class="badge badge-danger" id="error-telefono"></span>
        </div>
    </div>

    <div class="col-md-3">
        <div class="input-group mb-3 ">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
            </div>
            <input name="contacto"  class="form-control item-contacto" placeholder="Ingresar persona p/ contacto" aria-label="Nombre" aria-describedby="basic-addon1" id="exampleFormControlTextarea1" >
        </div>

        <div class="col-md-4">
            <span class="badge badge-danger" id="error-contacto"></span>
        </div>
    </div>



    <div class="col-md-4">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Horario contacto de: </span>
            </div>
            <input type="time" name="hora_inicio"  class="form-control item-hora_inicio" value="09:00" placeholder="horario" required aria-label="Nombre" aria-describedby="basic-addon1" id="exampleFormControlTextarea1" >
        </div>

        <div class="col-md-12">
            <span class="badge badge-danger" id="error-hora_inicio"></span>
        </div>
    </div>

    <div class="col-md-2">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"> a </span>
            </div>
            <input type="time" name="hora_fin"  class="form-control item-hora_fin" placeholder="horario"   value="20:00"  required aria-label="Nombre" aria-describedby="basic-addon1" id="exampleFormControlTextarea1" >
        </div>

        <div class="col-md-12">
            <span class="badge badge-danger" id="error-hora_fin"></span>
        </div>
    </div>
    <div class="col-md-12" align="center">
        <p style="color: grey">*En caso que cuente con internet o que su problema lo requiera, ingresar los datos de team viewer de su equipo* </p>
    </div>
    <br>


    <div class="col-md-6">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"> <i class="fas fa-server"></i> </span>
            </div>
            <input type="text" name="teamviewer_id"  class="form-control item-teamviewer_id" placeholder="Id teamviewer"  aria-label="Nombre" aria-describedby="basic-addon1" id="exampleFormControlTextarea1" >
        </div>

        <div class="col-md-12">
            <span class="badge badge-danger" id="error-teamviewer_id"></span>
        </div>
    </div>

    <div class="col-md-6">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"> <i class="fas fa-unlock-alt"></i> </span>
            </div>
            <input type="text" name="teamviewer_password"  class="form-control item-teamviewer_password" placeholder="Contraseña teamviewer"  aria-label="Nombre" aria-describedby="basic-addon1" id="exampleFormControlTextarea1" >
        </div>

        <div class="col-md-12">
            <span class="badge badge-danger" id="error-teamviewer_password"></span>
        </div>
    </div>
    <div class="col-md-12">
        <button class="btn btn-success agregarCampos">Agregar archivo</button>
        <div id="campos"></div>
    </div>



</div>