<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="fas fa-align-justify"></i></span>
    </div>
    <input type="text" name="name"  class="form-control item-name" placeholder="Ingresar nombre sucursal" required aria-label="Nombre" aria-describedby="basic-addon1">
</div>
<div class="col-md-12">
    <span class="badge badge-danger" id="error-name"></span>
</div>
<input type="hidden" class="rutas" name="rutas">
<input type="hidden" name="id"  class="form-control item-id">
