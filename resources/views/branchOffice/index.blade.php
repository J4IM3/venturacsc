@extends('layouts.app')
@section('content')
    <div class="container">
        <h2>Listado sucursales</h2>
        <br>
        <button class="btn btn-primary new-branch-office">Nueva sucursal</button>
        <br><br>
        <table id="table_branchOffice" class="display">
        </table>
        @include('branchOffice.modal-new-branch-office')

    </div>
@endsection