<div class="col-md-12 row">
    <div class="col-md-12">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-exclamation-circle"></i></span>
            </div>
            <input type="text" class="form-control" name="problema" required placeholder="Ingresar problema" id="item-problema">
        </div>
        <div class="col-md-12">
            <span class="badge badge-danger" id="error-problema"></span>
        </div>
        <input type="hidden" class="form-control rutas" name="rutas">
        <input type="hidden" class="form-control id" name="id">
    </div>

</div>