@extends('layouts.app')
@section('content')
    <div class="container">
        <h2>Listado problemas</h2>
        <button class="btn btn-primary problama-nuevo">Crear nuevo</button>
        <br> <br>
        <table id="problems_table"></table>
        @include('problemas.moda-create-problem')
    </div>
    @endsection