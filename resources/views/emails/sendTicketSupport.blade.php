@component('mail::message')
El usuario {{ $data['userName']}}  creo el ticket numero  {{ $data['id']}}<br>

<br>
Sucursal: {{ $data['sucursal'] }}
<br>
Problema: {{ $data['problema'] }}
<br>
Descripción: {{ $data['descripcion'] }}
<br>

@component('mail::button', ['url' => 'http://soporte.venturacsc.com/view-ticket/'.$data['slug']])
Ir a ticket
@endcomponent

<p style="color: grey; size: 15px">NO responder a este email ,ya que se envia automaticamente.</p>
<br>
{{ config('app.name') }}
@endcomponent
