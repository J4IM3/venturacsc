@component('mail::message')
    El usuario {{ $user->name }}  ah respondido al ticket  {{ $ticket->slug}} <br>

    Respuesta:
    {{ $comment }}



    @component('mail::button', ['url' => 'http://soporte.venturacsc.com/view-ticket/'.$ticket->slug])
    Puedes responderle aqui:
    <br><br>
    @component('mail::button', ['url' => 'http://soporte.venturacsc.com/finish-ticket/'.$ticket->slug])
        O dar por finalizado el ticket

@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
