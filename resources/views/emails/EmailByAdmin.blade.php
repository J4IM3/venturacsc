@component('mail::message')
Hola {{ $userName }}:
<br>
{{ $admin }} creo el ticket {{ $ticket->id }}, ya que se reporto el siguiente problema:
<br>
{{ $problema }}.
<br>
{{ $ticket->descripcion }}
<br>
Este ticket se encuentra en revisión, para mas detalles puedes consultar el estatus en el siguiente link.
@component('mail::button', ['url' => 'http://soporte.venturacsc.com/'.$ticket->slug])
    Detalle ticket
@endcomponent

Para dar por finalizado este servicio , dar click aqui:
@component('mail::button', ['url' => 'http://soporte.venturacsc.com/finish-ticket/'.$ticket->slug])
    Finalizar ticket
@endcomponent


Gracias ,<br>
Soporte {{ config('app.name') }}
@endcomponent
