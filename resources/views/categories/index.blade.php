@extends('layouts.app')
@section('content')
    <div class="container">
        <h2>Categorias</h2>
        <br>
        <button class="btn btn-primary create-categorie">Nuevo</button>
        <br> <br>
        <table id="table_categories">

        </table>
        @include('categories.modal-new-category')
    </div>
    @endsection