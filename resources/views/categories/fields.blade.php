<div class="col-md-12 row">

    <div class="col-md-12">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-list"></i></span>
            </div>
            <input type="text" class="form-control" name="name" required placeholder="Ingresar nombre de la categoria" id="item-name">
        </div>
        <div class="col-md-12">
            <span class="badge badge-danger" id="error-name"></span>
        </div>
    </div>

    <div class="col-md-12">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-list"></i></span>
            </div>
            <input type="text" class="form-control" name="description" required placeholder="Ingresar descripcion de la categoria" id="item-description">
        </div>
        <div class="col-md-12">
            <span class="badge badge-danger" id="error-description"></span>
        </div>
    </div>
    <input type="hidden" name="ruta" class="ruta">
    <input type="hidden" name="id" id="item-id">

</div>