<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">



</head>
<style>
    .navbar{
        font-size: 18px;
    }
    .nav-link {
        color: white !important;
    }
</style>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel" style="background-color: #BF0411; color: white;">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="https://www.laproveedora.com/images/logo-white.png" width="180px" height="80px" alt="">
            </a>
            <button class="navbar-toggler ml-auto custom-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navba -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest


                    @else

                        @can('admin.index')
                            <li class="nav-item active">
                                <a class="nav-link" href="{{ route('get-dashboard') }}">Dashboard Admin<span class="sr-only">(current)</span></a>
                            </li>
                        @endcan

                    <!--
                            @can('users.index')
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Usuario <span class="sr-only">(current)</span></a>
                                </li>
                            @endcan
                        -->

                        @can('admin.index')
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Sistema
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                                    <a class="dropdown-item" href="{{ route('users') }}">
                                        <span style="font-size: 20px; color: Dodgerblue;">
                                            <i class="fas fa-users"></i>
                                        </span>&nbsp Usuarios
                                    </a>


                                    <a class="dropdown-item" href="/branch_office">
                                    <span style="font-size: 20px; color: Dodgerblue;">
                                            <i class="fas fa-store"></i>
                                        </span>&nbsp Sucursales
                                    </a>

                                    <a class="dropdown-item" href="/department">
                                    <span style="font-size: 20px; color: Dodgerblue;">
                                            <i class="fas fa-chalkboard-teacher"></i>
                                        </span>&nbsp Departamentos
                                    </a>

                                        <a class="dropdown-item" href="/categories">
                                    <span style="font-size: 20px; color: Dodgerblue;">
                                            <i class="fas fa-clipboard-list"></i>
                                        </span>&nbsp Categorias
                                        </a>

                                    <a class="dropdown-item" href="/problemas">
                                    <span style="font-size: 20px; color: Dodgerblue;">
                                            <i class="fas fa-list"></i>
                                        </span>&nbsp Problemas
                                    </a>

                                </div>
                            </li>
                        @endcan


                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Soporte
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                                @can('users.dashboard-tickets')
                                    <a class="dropdown-item" href="{{route('dashboard-tickets')}}">
                                        <span style="font-size: 20px; color: Dodgerblue;">
                                            <i class="fas fa-clipboard-list"></i>
                                        </span>&nbsp Mis tickets
                                    </a>
                                @endcan


                                    @can('admin.index')
                                        <a class="dropdown-item" href="/dashboard-support">
                                    <span style="font-size: 20px; color: Dodgerblue;">
                                            <i class="fas fa-clipboard-list"></i>
                                        </span>&nbsp Listado tickets
                                        </a>
                                    @endcan


                            </div>
                        </li>



                            <li class="nav-item dropdown">

                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>


                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('user-configuration') }}"><span style="font-size: 20px; color: Dodgerblue;">
                                            <i class="fas fa-users-cog"></i>
                                        </span>&nbsp&nbsp;Configuración usuario</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                        </a>


                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

                                </div>
                            </li>
                        &nbsp; &nbsp;<img src="{{ asset('images/users/'.Auth::user()->imagen_name) }}" class="avatar" alt='Avatar'>

                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    <nav class="fixed-bottom navbar-light bg-light">
        <div class="text-center">
            © Derechos reservados TI VenturaCSC - La Proveedora
        </div>

    </div>
</body>
</html>
