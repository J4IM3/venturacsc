@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">

            <div class="card text-center col-md-4">
                <form action="{{ route('form-update-image') }}" autocomplete="off" method="POST" enctype="multipart/form-data">
                    @csrf
                <div class="card-body row"  style="text-align: center;">
                    <h5 class="card-title"></h5>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="img"></label>
                            <img src="{{asset($user->imagen.$user->imagen_name)}}" alt="Imagen " class="avatarConfig" width="200px" height="200px">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="file" name="file" value="Buscar    ">
                            <input type="hidden" name="id" value="{{ $user->id }}" >
                        </div>
                    </div>
                    <div class="card-footer bg-transparent text-muted border-success col-md-12">
                        <div class="form-group">
                            <button class="btn btn-primary btn-block" type="submit">Cambiar imagen</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>

            <div class="card text-center col-md-4">

                <form action="{{ route('form-update-user',[$user->id,] ) }}" autocomplete="off" method="POST" >
                    @csrf
                    <div class="card-body" >
                        <h5 class="card-title">Datos del usuario</h5>
                        <br><br>
                            <input type="hidden" class="form-control " name="id" value="{{old('id', $user->id)}}">
                            <div class="form-group row">
                                <label for="username " class="col-md-4 col-form-label">Nombre</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="name" value="{{old('name', $user->name)}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="username" class="col-md-4 col-form-label">Email</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control " name="email" value="{{old('email', $user->email)}}">
                                </div>
                            </div>

                        <div class="card-footer bg-transparent text-muted  border-success col-md-12">
                            <div class="form-group">
                                <button class="btn btn-primary btn-block" type="submit">Actualizar datos</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="card text-center col-md-4">
                    <div class="card-body">
                        <div class="card-footer bg-transparent text-muted  border-success col-md-12">
                            <button class="btn btn-primary update-password btn-block">Cambiar contraseña</button>
                            @include('users.change-password')
                        </div>º
                    </div>
            </div>


            <div class="card text-center col-md-12">
                    <div class="card-body row" >
                        <h5 class="card-title"></h5>
                        <div class="col-md-12">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                                </div>
                            @endif
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                        </div>


                    </div>

                </form>
            </div>


        </div>
    </div>
    @endsection

