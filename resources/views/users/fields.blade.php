<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
    </div>
    <input type="text" name="name"  class="form-control item-name" placeholder="Ingresar nombre" required aria-label="Nombre" aria-describedby="basic-addon1">
</div>
<div class="col-md-12">
    <span class="badge badge-danger" id="error-name"></span>
</div>

<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="fas fa-at"></i></span>
    </div>
    <input type="email" name="email"  class="form-control item-email" placeholder="Ingresar email" required aria-describedby="basic-addon1">
</div>
<div class="col-md-12">
    <span class="badge badge-danger" id="error-email"></span>
</div>

<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="far fa-hand-point-right"></i></span>
    </div>
    <select name="branchOffice_id" id="item-branchOffice_id" class="form-control item-branchOffice_id" style="width: 50%" required>
        <option value="">Seleccionar sucursal</option>
        @foreach($branchOffices as $suc)
        <option value="{{ $suc->id }}">{{ $suc->name }}</option>
            @endforeach
    </select>

</div>
<div class="col-md-12">
    <span class="badge badge-danger" id="error-branchOffice_id"></span>
</div>

<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="far fa-hand-point-right"></i></span>
    </div>
    <select name="department_id" id="item-department_id" class="form-control item-department_id" required>
        <option value="">Seleccionar departamento</option>
        @foreach($departments as $dep)
            <option value="{{ $dep->id }}">{{ $dep->name }}</option>
        @endforeach
    </select>

    &nbsp; <button class="btn btn-primary btn add_deppartent"><i class="far fa-plus-square"></i></button>

</div>
<div class="col-md-12">
    <span class="badge badge-danger" id="error-department_id"></span>
</div>


<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="far fa-hand-point-right"></i></span>
    </div>
    <select name="type" id="item-type" class="form-control" required>
        <option value="">Seleccionar rol</option>
        <option value="user">Usuario</option>
        <option value="ti">Administrador</option>
    </select>

</div>
<div class="col-md-12">
    <span class="badge badge-danger" id="error-type"></span>
</div>



<div class="input-group mb-3 section-password">
    <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"><i class="fas fa-user-secret"></i></span>
    </div>
    <input type="password" name="password"  class="form-control item-name" required placeholder="Ingresar contraseña" aria-describedby="basic-addon1">
</div>
<div class="col-md-12">
    <span class="badge badge-danger" id="error-password"></span>
</div>

<input type="hidden" class="rutas" name="rutas">
<input type="hidden" name="id"  class="form-control item-id">
