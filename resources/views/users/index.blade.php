@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Lista de usuarios</h1>
        @can('admin.create')
            <button class="btn btn-success new-user">Nuevo usuario</button>
        @endcan
        <br><br>
        <table id="table_users" class="display">
        </table>
        @include('users.modal-users')
        @include('users.add-department')
    </div>
    @endsection