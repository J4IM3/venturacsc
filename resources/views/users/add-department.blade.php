<div class="modal fade" id="modal-addDepartment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><p class="titleModal"></p></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="form-save-department" class="form-add-user" autocomplete="off">
                <div class="modal-body">


                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-city"></i></span>
                        </div>
                        <input type="text" name="email"  class="form-control item-department-add" placeholder="Ingresar departamento" required aria-describedby="basic-addon1">
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary send-data-department">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>