<div class="modal fade" id="change-password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><p class="titleModal"></p></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="form-change-password" class="form-add-user" autocomplete="off">
                <div class="modal-body">

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user-secret"></i></span>
                        </div>
                        <input type="password" name="password"  class="form-control item-password" placeholder="Ingresa tu nueva contraseña" required aria-label="Nombre" aria-describedby="basic-addon1">
                    </div>
                    <div class="col-md-12">
                        <span class="badge badge-danger" id="error-password"></span>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user-secret"></i></span>
                        </div>
                        <input type="password" name="password_confirmation"  class="form-control item-password_confirmation" placeholder="Confirma tu contraseña" required aria-describedby="basic-addon1">
                    </div>
                    <div class="col-md-12">
                        <span class="badge badge-danger" id="error-password_confirmation"></span>
                    </div>
                    <div class="col-md-12">
                        <span class="badge badge-danger" id="error-validation"></span>
                    </div>
                    <input type="hidden" name="id" value="{{ $user->id }}"  class="form-control item-id">



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary send-data-user">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>