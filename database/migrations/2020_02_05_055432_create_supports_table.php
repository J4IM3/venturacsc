<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supports', function (Blueprint $table) {
            $table->bigIncrements(  'id');
            $table->integer('user_id');
            $table->string('telefono');
            $table->string('horario_contacto');
            $table->integer('problema_id');
            $table->string('descripcion')->nullable();
            $table->longText('solucion')->nullable();
            $table->string('teamviewer_id')->nullable();
            $table->string('teamviewer_password')->nullable();
            $table->integer('sucursal_id');
            $table->integer('department_id');
            $table->dateTime('fecha');
            $table->string('status');
            $table->string('contacto')->nullable();
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supports');
    }
}
