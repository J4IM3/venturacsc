<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableHistorialAssignments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_historial_assignments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug');
            $table->integer('assigned_user');
            $table->dateTime('fecha');
            $table->enum('type',['assigned','reassigned','no-assigned']);
            $table->integer('user_admin_reassigned');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_historial_assignments');
    }
}
