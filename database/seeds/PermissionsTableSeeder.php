<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;
class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'name' => 'Navegar usuarios',
            'slug' => 'admin.index',
            'description'=>'Mostrar el listado de usuarios'
        ]);

        Permission::create([
            'name' => 'Ver detalle de  usuarios',
            'slug' => 'admin.show',
            'description'=>'Ver el detalle de usuarios'
        ]);

        Permission::create([
            'name' => 'Editar usuarios',
            'slug' => 'admin.update',
            'description'=>'Editar cualquier dato de un usuarios'
        ]);

        Permission::create([
            'name' => 'Eliminar usuarios',
            'slug' => 'admin.destroy',
            'description'=>'Elimina un usuario del sistema'
        ]);

        Permission::create([
            'name' => 'Crear un usuario',
            'slug' => 'admin.create',
            'description'=>'Crea un usuario en el sistema '
        ]);

        Permission::create([
            'name' => 'Visualizar menu soporte tickets usuario',
            'slug' => 'users.index',
            'description'=>'Visualizar menu soporte tickets usuario'
        ]);


        //roles

        Permission::create([
            'name' => 'Navegar roles',
            'slug' => 'roles.index',
            'description'=>'Mostrar el listado de roles'
        ]);
         

        Permission::create([
            'name' => 'Ver detalle de roles',
            'slug' => 'roles.show',
            'description'=>'Ver el detalle de roles'
        ]);

        Permission::create([
            'name' => 'Editar roles',
            'slug' => 'roles.edit',
            'description'=>'Editar cualquier rol en el sistema'
        ]);

        Permission::create([
            'name' => 'Eliminar roles',
            'slug' => 'roles.destroy',
            'description'=>'Elimina un rol del sistema'
        ]);

        Permission::create([
            'name' => 'Crear un rol',
            'slug' => 'roles.create',
            'description'=>'Crea un rol en el sistema '
        ]);

    }
}
