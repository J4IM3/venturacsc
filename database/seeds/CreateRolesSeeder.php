<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;
class CreateRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'admin',
            'slug' => 'admin',
            'description' => 'Perfil administrador'
        ]);
        Role::create([
            'name' => 'Ti',
            'slug' => 'ti',
            'description' => 'Perfil standar ti'
        ]);
        Role::create([
            'name' => 'Usuario',
            'slug' => 'user',
            'description' => 'Perfil para usuarios'
        ]);
    }
}
