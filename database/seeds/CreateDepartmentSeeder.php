<?php

use Illuminate\Database\Seeder;
use App\Http\Entities\Department;
class CreateDepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::create([
           'name' => 'Tecnologias de información',
           'status' => 1
        ]);
    }
}
