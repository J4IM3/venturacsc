<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'admin',
            'email'=>'admin@admin.com',
            'password' => bcrypt("s3st2m1s"),
            'status' => 1,
            'branchOffice_id' => 1,
            'department_id' =>1,
            'type'=>"admin",
            'imagen'=>'/images/users/',
            'imagen_name'=>'profile.png'
        ]);
    }
}
