<?php

use Illuminate\Database\Seeder;
use App\Http\Entities\BranchOffice;
class BranchOfficeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BranchOffice::create([
            'name'=>'Matriz',
            'status'=>'1'
        ]);BranchOffice::create([
            'name'=>'Merced',
            'status'=>'1'
        ]);BranchOffice::create([
            'name'=>'Soledad',
            'status'=>'1'
        ]);BranchOffice::create([
            'name'=>'Santa Rosa',
            'status'=>'1'
        ]);BranchOffice::create([
            'name'=>'Cedis',
            'status'=>'1'
        ]);BranchOffice::create([
            'name'=>'Autoservicio',
            'status'=>'1'
        ]);BranchOffice::create([
            'name'=>'Xoxo',
            'status'=>'1'
        ]);
    }
}
