<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionsTableSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(BranchOfficeSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CreateRolesSeeder::class);
        $this->call(CreateDepartmentSeeder::class);


    }
}
