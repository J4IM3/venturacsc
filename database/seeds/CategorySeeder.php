<?php

use Illuminate\Database\Seeder;
use App\Http\Entities\Category;
class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name'=>'Soporte',
            'description'=>'Soporte'
        ]);

        Category::create([
            'name'=>'Desarrollo',
            'description'=>'Desarrollo'
        ]);
    }
}
