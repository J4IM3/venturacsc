<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-02-06
 * Time: 12:44
 */

Route::view('/categories','categories/index')->name('/categories');
Route::get('get-categories-ajax','CategoryController@getDataByAjax')->name('get-categories-ajax');
Route::post('save-category','CategoryController@save')->name('save-category');
Route::post('udpate-category/{id}','CategoryController@update')->name('udpate-category');