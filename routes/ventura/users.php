<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-01-13
 * Time: 23:36
 * Route::get('users',')->name('users');
 */

Route::get('users','UserController@index')->name('users')->middleware('can:admin.show');

Route::get('get-users-ajax','UserController@getUsersAjax')->name('get-users-ajax')->middleware('can:admin.index');
Route::post('create','UserController@create')->name('create')->middleware('can:admin.create');
Route::post('udpate/{id}','UserController@update')->name('update')->middleware('can:admin.update');

Route::get('user-configuration','UserController@configuration')->name('user-configuration');
Route::POST('form-update-user/{id}','UserController@updateUser')->name('form-update-user');
Route::POST('form-update-image','UserController@udpateImage')->name('form-update-image');
Route::POST('update-password','UserController@updatePassword')->name('update-password');
Route::POSt('changeStatus','UserController@changeStatus')->name('changeStatus');

//ir al dashboard
Route::get('get-dashboard','UserController@getDashboardData')->name('get-dashboard')->middleware('can:admin.dashboard');