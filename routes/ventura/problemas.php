<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-02-19
 * Time: 01:12
 */
Route::view('/problemas','problemas/index');
Route::get('get-users-problems','ProblemaController@getDataByAjax')->name('get-users-problems');
Route::post('create-problem','ProblemaController@create')->name('create-problem');
Route::post('udpate-problem/{id}','ProblemaController@update')->name('udpate-problem');
Route::post('changeStatus-problem','ProblemaController@changeStatus')->name('changeStatus-problem');