<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-01-25
 * Time: 01:29
 */
Route::view('/branch_office','branchOffice/index');
Route::GET('get-branchOffice-ajax','BranchOfficeController@getBranchOffice')->name('get-branchOffice-ajax');
Route::POST('new-branch-office','BranchOfficeController@nuevo')->name('new-branch-office');
Route::POST('udpate-branch-office/{id}','BranchOfficeController@update')->name('udpate-branch-office');
Route::POST('changeStatus-branchOffice','BranchOfficeController@changeStatus')->name('changeStatus-branchOffice');