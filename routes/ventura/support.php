<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-02-02
 * Time: 23:59
 */
Route::get('dashboard-tickets','SupportController@index')->name('dashboard-tickets');
Route::get('new-ticket','SupportController@create')->name('new-ticket');
Route::post('save-ticket','SupportController@save')->name('save-ticket');
Route::get('get-tickets-ajax','SupportController@getTicketsByAjax')->name('get-tickets-ajax');
Route::get('view-ticket/{slug}','SupportController@view')->name('view-ticket');
Route::POST('form-sent-comment','SupportController@saveComment')->name('form-sent-comment');

Route::get('dashboard-support','SupportController@dashboardSupport')->name('dashboard-support');
Route::get('get-list_tickets-ajax','SupportController@getListTicketsByAjax')->name('get-list-tickets-ajax');
Route::get('view-ticket-admin/{slug}','SupportController@viewTicketAdmin')->name('view-ticket-admin');

Route::get('finish-ticket/{slug}','SupportController@finishTicket')->name('finish-ticket');

Route::get('get-admins','SupportController@getUsersAdmin')->name('get-admins');
Route::post('save-user-assigned','SupportController@assignedUser')->name('save-user-assigned');
Route::post('save-ticket-pending','SupportController@saveTicketPending')->name('save-ticket-pending');
Route::post('save-ticket-finalize','SupportController@finalize')->name('save-ticket-finalize');
Route::POST('envio-respuesta','SupportController@respuesta')->name('envio-respuesta');
Route::post('save-calif-ajax','SupportController@saveCalifByAjax')->name('save-calif-ajax');
Route::POST('save-add-files','SupportController@addFiles')->name('save-add-files');

Route::post('get-branchOffices','SupportController@getBranchOffices')->name('get-branchOffices');
Route::post('save-ticket-by-admin','SupportController@saveTicketAdmin')->name('save-ticket-by-admin');