<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-01-26
 * Time: 02:41
 */
Route::view('/department','/department/index');
Route::get('get-departments-ajax','DepartmentController@getDepartmentsAjax')->name('get-departments-ajax');
Route::POST('create-department',     'DepartmentController@create')->name('create-department');
Route::POST('udpate-department/{id}','DepartmentController@update')->name('udpate-department');
Route::POST('changeStatus-department','DepartmentController@changeStatus')->name('changeStatus-department');