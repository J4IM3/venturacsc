<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();
Route::group(['middleware' => 'auth'], function() {

    Route::get('/home', 'HomeController@index')->name('home');
    include_once 'ventura/users.php';
    include_once 'ventura/branch_office.php';
    include_once 'ventura/deparment.php';
    include_once 'ventura/support.php';
    include_once 'ventura/categories.php';
    include_once 'ventura/problemas.php';

});