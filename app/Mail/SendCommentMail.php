<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendCommentMail extends Mailable
{
    use Queueable, SerializesModels;
    public  $user;
    public  $ticket;
    public $comment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$ticket,$comment)
    {
        //
        $this->user = $user;
        $this->ticket = $ticket;
        $this->comment = $comment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.send-comment')
            ->subject('Hay una respuesta en tu ticket ')
            ->with('user', $this->user)
            ->with('ticket', $this->ticket)
            ->with('comment',$this->comment);
    }
}
