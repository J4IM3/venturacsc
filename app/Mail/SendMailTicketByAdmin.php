<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailTicketByAdmin extends Mailable
{
    use Queueable, SerializesModels;
    public $userName;
    public $ticket;
    public $adminName;
    public $problema;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userName,$ticket,$adminName,$problema)
    {
        //
        $this->userName = $userName;
        $this->ticket = $ticket;
        $this->adminName = $adminName;
        $this->problema = $problema;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.EmailByAdmin')
            ->subject('Se creo un nuevo ticket')
            ->with('userName',$this->userName)
            ->with('ticket',$this->ticket)
            ->with('admin',$this->adminName)
            ->with('problema',$this->problema);
    }
}
