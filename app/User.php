<?php

namespace App;

use App\Http\Entities\BranchOffice;
use App\Http\Entities\Comments;
use App\Http\Entities\Historial_Asignacion;
use App\Http\Entities\Support;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Caffeinated\Shinobi\Concerns\HasRolesAndPermissions;


class User extends Authenticatable
{
    use Notifiable,HasRolesAndPermissions;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','status','imagen','imagen_name','branchOffice_id','department_id','type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function usersComments()
    {
        return $this->belongsTo(Comments::class);
    }

    public function usersTicket()
    {
        return $this
            ->belongsTo(Support::class);
    }
    public function usersHistorial()
    {
        return $this
            ->belongsTo(Historial_Asignacion::class);
    }

    public function history_assignations()
    {
        return $this
            ->belongsTo(Historial_Asignacion::class);
    }
    public function userSucursal()
    {
        return $this
            ->hasOne(BranchOffice::class,'id','branchOffice_id');
    }
}
