<?php

namespace App\Http\Controllers;

use App\Http\Entities\BranchOffice;
use App\Http\Entities\Department;
use App\Http\Entities\HistorialAsignacion;
use App\Http\Entities\Support;
use App\Http\Repositories\UserRepo;
use App\Http\Request\EditUserRequest;
use App\Http\Request\RequestUpdateUserConfig;
use App\Http\Request\SaveUser;
use App\Http\Request\RequestUpdatePassword;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Console\Input\Input;
use Yajra\DataTables\DataTables;
use Intervention\Image\ImageManagerStatic as Image;
use Caffeinated\Shinobi\Concerns\HasRolesAndPermissions;

class UserController extends Controller
{

    public function __construct(UserRepo $userRepo)
    {
        $this->middleware('auth');
        $this->userRepo = $userRepo;
    }

    public function index()
    {
        $branchOffices = BranchOffice::where('status',1)->get();
        $departments = Department::where('status',1)->get();
        return view('users.index',compact('branchOffices','departments'));
    }

    /**
     * Retorna json a la tabla de usuarios via ajax
     *
     */

    public function getUsersAjax()
    {
        $users = User::all();
        return DataTables::of($users)->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(SaveUser $request)
    {
        return $this->userRepo->save($request->all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateUser(RequestUpdateUserConfig $request)
    {
        return $this->userRepo->updateUser($request->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id,EditUserRequest $request)
    {
        return $this->userRepo->update($request->all());
    }

    public function configuration()
    {
        $user = User::where('id',Auth::user()->id)->first();
        return view('users.configuration',compact('user'));
    }

    /**
     * Actualizar avatar user
     */
    public function udpateImage(Request $request)
    {
        $file = $request->file('file');
        $name = $request->get('id').date('Ymd H:i:s').$file->getClientOriginalName();
        Image::make($file)->resize(300, 200)->save('images/users/'.$name);
        $user = User::where('id',$request->get('id'))->first();
        $user->imagen_name = $name;
        if ($user->save())
        {
            return back()->with('success','Tus datos han sido actualizados');
        }
    }

    public function updatePassword(RequestUpdatePassword $request)
    {
        return $this->userRepo->changePassword($request->all());
    }

    /**
     * Funcion que recibe los parametros para hacer el cambio del status del usuario
     */

    public function changeStatus(Request $request)
    {
        return $this->userRepo->changeStatus($request->get('id'));
    }

    /**
     * Envia a la vista dashboard los datos estadisticos del utlimo mes en cuanto a tickets
     *atentidos y el total de tickets
     */

    public function getDashboardData()
    {
        $month = date('m');
        $year = date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        $month = date('m');
        $year = date('Y');
        $primero = date('Y-m-d', mktime(0,0,0, $month, 1, $year));
        $ultimo = date('Y-m-d', mktime(0,0,0, $month, $day, $year));


        $ticketsFinalizados= Support::select('slug')->where('status','Finalizado')
            ->whereBetween('fecha',[$primero,$ultimo])
            ->get();
        $ticketsFinalizadosPorUsuario = HistorialAsignacion::whereIn('slug',$ticketsFinalizados)
            ->where('assigned_user',Auth::id())
            ->where('type',"assigned")
            ->count();
        $ticketsPendientes = $ticketsFinalizados= Support::select('slug')->where('status','Recibido')
            ->whereBetween('fecha',[$primero,$ultimo])
            ->get();
        $ticketsPendientesPorUsuario = HistorialAsignacion::whereIn('slug',$ticketsFinalizados)
            ->where('assigned_user',Auth::id())
            ->where('type',"assigned")
            ->count();
        $totalTickets = Support::whereBetween('fecha',[$primero,$ultimo])
            ->count();
        return view('inicio.dashboard-admin',compact('ticketsFinalizadosPorUsuario','ticketsPendientesPorUsuario','totalTickets'));
    }
}
