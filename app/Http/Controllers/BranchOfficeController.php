<?php

namespace App\Http\Controllers;
use App\Http\Entities\BranchOffice;
use App\Http\Repositories\BranchOfficeRepo;
use App\Http\Request\BranchOfficeRequest;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables as DataTables;
class BranchOfficeController extends Controller
{
    /**
     * @var BranchOfficeRepo
     */
    private $branchOfficeRepo;

    public  function __construct(BranchOfficeRepo $branchOfficeRepo)
    {
        $this->branchOfficeRepo = $branchOfficeRepo;
    }

    public function getBranchOffice()
    {
        $branchOffice = BranchOffice::all();
        return DataTables::of($branchOffice)->make(true);
    }

    /**
     * Nueva sucursal
     */

    public function nuevo(BranchOfficeRequest $request)
    {
        return $this->branchOfficeRepo->nuevo($request->get('name'));
    }

    public function update($id,Request $request){
        return $this->branchOfficeRepo->update($request->all());
    }

    public function changeStatus(Request $request)
    {
        return $this->branchOfficeRepo->changeStatus($request->get('id'));
    }


}
