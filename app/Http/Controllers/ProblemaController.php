<?php

namespace App\Http\Controllers;

use App\Http\Entities\Problema;
use App\Http\Request\RequestProblems;
use App\Http\Services\ServiceStatus;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class ProblemaController extends Controller
{
    /**
     * @var ServiceStatus
     */
    private $serviceStatus;

    public function __construct(ServiceStatus $serviceStatus)
    {
        $this->serviceStatus = $serviceStatus;
    }

    public function getDataByAjax()
    {
        $problemas = Problema::all();
        return DataTables::of($problemas)->make('true');
    }

    public function create(RequestProblems $request)
    {
        $problems = Problema::create([
            'problema' => $request->get('problema'),
            'status' => 1
        ]);

        if (!empty($problems)) {
            return ['success' => 'success', 'msg' => 'Registro creado correctamente'];
        }
        return ['success' => 'error', 'msg' => 'Ocurrio un error'];

    }

    public function update(RequestProblems $request)
    {
        $problema = Problema::where('id',$request->get('id'))->first();
        $problema->problema = $request->get('problema');
        if ($problema->save())
        {
            return ['success' => 'success', 'msg' => 'Registro actualizado'];
        }
        return ['success' => 'error', 'msg' => 'Ocurrio un error'];
    }

    public function changeStatus(Request $request)
    {
        $problem = Problema::where('id',$request->get('id'))->first();
        $status = $this->serviceStatus->changeStatus($problem->status);
        $problem->status = $status;
        if ($problem->save())
        {
            return ['success' => 'success', 'msg' => 'Estatus actualizado'];
        }
        return ['success' => 'error', 'msg' => 'Ocurrio un error'];
    }
}