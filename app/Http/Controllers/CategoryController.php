<?php

namespace App\Http\Controllers;

use App\Http\Entities\Category;
use App\Http\Repositories\CategoryRepo;
use App\Http\Request\RequestCreateCategory;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepo
     */
    private $categoryRepo;

    public function __construct(CategoryRepo $categoryRepo)
    {
        $this->categoryRepo = $categoryRepo;
    }

    public function getDataByAjax()
    {
        $categories = Category::all();
        return DataTables::of($categories)->make(true);
    }

    public function save(RequestCreateCategory $request)
    {
        return $this->categoryRepo->save($request->all());
    }

    public function update(Request $request)
    {
        return $this->categoryRepo->update($request->all());
    }
}
