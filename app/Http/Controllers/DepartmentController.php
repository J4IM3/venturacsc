<?php

namespace App\Http\Controllers;

use App\Http\Entities\Department;
use App\Http\Repositories\DepartmentRepo;
use App\Http\Request\DepartmentRequest;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables as DataTables;
class DepartmentController extends Controller
{
    /**
     * @var DepartmentRepo
     */
    private $departmentRepo;

    public  function __construct(DepartmentRepo $departmentRepo)
    {
        $this->departmentRepo = $departmentRepo;
    }

    public function getDepartmentsAjax()
    {
        $departments = Department::all();
        return DataTables::of($departments)->make(true);
    }

    public function create(DepartmentRequest $request)
    {
        return  $this->departmentRepo->create($request->get('name'));
    }

    public function update(DepartmentRequest $request)
    {
        return $this->departmentRepo->update($request->all());
    }

    public function changeStatus(Request $request)
    {
        return $this->departmentRepo->changeStatus($request->get('id'));
    }

}
