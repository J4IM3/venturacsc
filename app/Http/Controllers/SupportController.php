<?php

namespace App\Http\Controllers;

use App\Http\Entities\BranchOffice;
use App\Http\Entities\Calificacion;
use App\Http\Entities\Category;
use App\Http\Entities\Comments;
use App\Http\Entities\HistoryStatus;
use App\Http\Entities\HistoryTicket;
use App\Http\Entities\Problema;
use App\Http\Entities\Support;
use App\Http\Entities\TicketsPendiente;
use App\Http\Repositories\SupportRepo;
use App\Http\Request\RequestCreateTicket;
use App\User ;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use App\Http\Entities\HistorialAsignacion;

class SupportController extends Controller
{
    /**
     * @var SupportRepo
     */
    private $supportRepo;

    public function __construct(SupportRepo $supportRepo)
    {
        $this->supportRepo = $supportRepo;
    }

    public function index()
    {
        $problemas = Problema::all();
        $categories = Category::all();
        return view('support.index',compact('categories','problemas'));
    }

    public function create()
    {
        $problemas = Problema::all();
        return view('support.create',compact('problemas'));
    }

    public function save(RequestCreateTicket $request)
    {
        return $this->supportRepo->save($request->all());
    }

    /**
     *
     */
    public function getTicketsByAjax()
    {
        $tickets = Support::with('ticketsProblemas','usersTicket')
            ->where('user_id',Auth::id())
            ->orderBy('id','DESC')
            ->get();


        foreach ($tickets as $ticket)
        {
            $user_assigned = HistorialAsignacion::with('usersHistorial')
                ->where('slug',$ticket->slug)
                ->orderBy('id', 'DESC')->first();
            if (!empty($user_assigned->usersHistorial))
            {
                $asignado = $user_assigned->usersHistorial->name;
                $ticket->asignado = $asignado;
            }else{
                $ticket->asignado = "Pendiente";
            }
        }

        return DataTables::of($tickets)->make(true);
    }

    /**
     * Muestra la vista con los datos del ticket solicitado mediante el slug
     */

    public function view($slug)
    {
        if (Auth::guest())
        {
            return redirect()->route('login');
        }
        else
            {
                $user = User::where('id',Auth::id())->first();

                if ($user->type=="ti")
                {
                    $asignado = HistorialAsignacion::where('slug',$slug)->first();

                    if (empty($asignado))
                    {
                        $this->supportRepo->saveAssigned($slug,$asignado);
                    }
                }
                $ticket = Support::with('usersTicket','ticketsProblemas','sucursalTicket','department_ticket','ticketFile')->where('slug',$slug)->get();
                $adminSupport = HistorialAsignacion::with('history_assignations')->where('slug',$ticket[0]->slug)->get()->last();

                $comments = Comments::with('usersComments')
                    ->where('slug',$slug)
                    ->get();
                return view('support.view-ticket',compact('ticket','comments','adminSupport'));

            }


    }

    /**
     * Se encarga de recibir y guardar los comentarios de un ticket
     */

    public function saveComment(Request $request)
    {
        return $this->supportRepo->saveComment($request->all());
    }

    /**
     * retorna el listado de tickets para su visualizacion de parte del dpto de TI
     */

    public function getListTicketsByAjax()
    {
        $tickets = Support::with('ticketsProblemas','usersTicket.userSucursal')->orderBy('id','DESC')->get();
        if (!empty($tickets[0]))
        {
            $user_assigned = HistorialAsignacion::with('usersHistorial')
                ->where('slug',$tickets[0]->slug)
                ->orderBy('id', 'DESC')->first();
        }


        foreach ($tickets as $ticket)
        {
            $user_assigned = HistorialAsignacion::with('usersHistorial')
                ->where('slug',$ticket->slug)
                ->orderBy('id', 'DESC')->first();
            if (!empty($user_assigned->usersHistorial))
            {
                $asignado = $user_assigned->usersHistorial->name;
                $ticket->asignado = $asignado;
            }else{
                $ticket->asignado = "Pendiente";
            }
        }
        return DataTables::of($tickets)->make(true);
    }

    /**
     * El usuario de ti entra a ver el detalle del tickt y automaticamente se le asigna dicho ticket
     */

    public function viewTicketAdmin($slug)
    {

        $asignado = HistorialAsignacion::where('slug',$slug)->first();
        if (empty($asignado))
        {
            $this->supportRepo->saveAssigned($slug,$asignado);
        }
        return redirect()->route('view-ticket', ['slug' => $slug]);

    }

    public function finishTicket($slug)
    {
       return  $this->supportRepo->finalizeTicket($slug);
    }

    public function dashboardSupport()
    {
        $sucursales = BranchOffice::where('status',1)->get();
        $problemas = Problema::where('status',1)->get();
        $usersAdmin = User::where('type','ti')
            ->where('status',1)
            ->get();
        return view('support.dashboard-tickets',compact('usersAdmin','sucursales','problemas'));
    }

    /**
     * El administrador asigna el ticket a un usuario
     */

      public function assignedUser(Request $request)
      {
        $ticket = Support::where('id',$request->get('num_ticket'))->first();
        return $this->supportRepo->saveAssignedByAdmin($ticket,$request->get('user_assigned'));
      }

      /**
       * Coloca un ticket como pendiente
       */

      public function saveTicketPending(Request $request)
      {
          $ticketsPendientes = TicketsPendiente::create([
              'slug' => $request->get('slug'),
              'motivo' => $request->get('motivo'),
              'fecha' => $request->get('fecha')
          ]);

          Comments::create([
              'slug' => $request->get('slug'),
              'comment' => $request->get('motivo') . " - Fecha de solucion aproximada: " . $request->get('fecha'),
              'fecha' => date(now()),
              'user_id' => Auth::id()
          ]);

          $ticket = Support::where('slug', $request->get('slug'))->first();
          $ticket->status = "Pendiente";
          if ($ticket->save())
          {
              return ['success' =>'success','msg'=> "Se cambio el status a pendiente correctamente "];
          }

      }

      public function finalize(Request $request)
      {
          return $this->supportRepo->finalize($request->get('slug'));
      }

    /**
     * Recibe los resultados de la encuesta al finalizar un ticket
     */
      public function respuesta(Request $request)
      {
          $this->supportRepo->saveCalif($request->all());
          return redirect()->route('dashboard-tickets');
      }

      public function saveCalifByAjax(Request $request)
      {
          $this->supportRepo->saveCalif($request->all());
          return ['success'=> 'success','msg'=>'Ticket finalizado'];
      }

      /**
       * Recibe uno o multiples archivos para guardar en un ticket
       */
      public function addFiles(Request $request)
      {
        return $this->supportRepo->saveAddFiles($request->all());
      }

      public function getBranchOffices(Request $request)
      {
          return User::where('branchOffice_id',$request->get('sucursal_id'))->get();
      }

      /**
       * Recibe los parametros para crear un ticket desde el lado del administrador
       */

      public function saveTicketAdmin(Request $request)
      {
          return $this->supportRepo->saveTicketAdmin($request->all());
      }
}
