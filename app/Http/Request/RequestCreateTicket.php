<?php

namespace App\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

class RequestCreateTicket extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'problema_id' => 'required|max:80',
            'descripcion' => 'max:200',
            'telefono'=>'required',
            'contacto'=>'max:80',
            'hora_inicio'=>'required|after:8:59',
            'hora_fin'=>'required|after_or_equal:hora_inicio|before:20:30',
        ];
    }

    public function messages()
    {
        return [
            'problema_id.required' => 'El campo es requerido',
            'problema_id.max' =>'No debes exceder 80 caracteres,para mas espacio utiliza el campo descripcion' ,
            'telefono.required' => 'Debes ingresar un numero telefonico donde contactarte',
            'hora_inicio.required' => 'Ingresa el horario en el cual podemos contactarte',
            'hora_inicio.after' => 'Nuestro horario de atencion es a partir de las 9 am ',
            'hora_fin.required'  => 'Ingresa el horario en el cual podemos contactarte',
            'hora_fin.after_or_equal' => 'El horario final debe ser mayor al horario inicial ',
            'hora_fin.before' => 'Nuestro horario de atencion es hasta las 8:30pm ',

        ];
    }
}
