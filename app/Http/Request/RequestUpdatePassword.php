<?php

namespace App\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

class RequestUpdatePassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6|'
        ];
    }

    public function messages()
    {
        return [
            'password.required' => "El password es obligatorio",
            'password_confirmation.required' => "El password de confirmacion es obligatorio",
            'password.min' => "El password debe tener al menos 6 caracteres",
            'password.confirmed' => "El campo password y confirmacion de password son diferentes , deben ser iguales",
            'password_confirmation.min' => "El password debe tener al menos 6 caracteres",
        ];
    }
}
