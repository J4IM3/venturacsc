<?php

namespace App\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

class DepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required | max:30'
        ];
    }

    public function messages()
    {
        return[
            'name.required' => 'Este campo es requerido',
            'name.max'=>'El numero maximo de caracteres es de 30'
        ];
    }
}
