<?php

namespace App\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

class RequestCreateCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required | max: 80'
        ];

    }

    public function messages()
    {
        return [
          'name.required'=>'Debe contener un valor',
          'name.max' => 'No debe ser mayor de 80 caracteres'
        ];
    }
}
