<?php

namespace App\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

class RequestUpdateUserConfig extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required  | max:80',
            'email' => 'required | unique:users,email,'.$this->id,
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'Introducir el nombre es obligatorio',
            'name.max' => 'El numero de caracteres permitidos es de 80',
            'email.required'=> 'El email es obligatorio',
            'email.unique' => 'Este email ya esta en uso'
        ];
    }
}
