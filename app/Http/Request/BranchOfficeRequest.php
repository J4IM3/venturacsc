<?php

namespace App\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

class BranchOfficeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required | max:50'
        ];
    }

    public function messages()
    {
        return[
            'name.required' => 'Campo requerido',
            'name.max' => 'El nombre no puede exceder de 50 caracteres'
        ];
    }
}
