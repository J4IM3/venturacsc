<?php

namespace App\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

class RequestProblems extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'problema' => 'required | max:240'
        ];
    }

    public function messages()
    {
        return [
            'problema.required' => "Este campo es requerido",
            'problema.max' => "El numero de caracteres debe ser o menor a 240 ",
            'problema.unique' => "El problema debe ser unico"
        ];
    }
}
