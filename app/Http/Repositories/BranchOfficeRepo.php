<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-01-25
 * Time: 23:56
 */

namespace App\Http\Repositories;
use App\Http\Entities\BranchOffice;
use App\Http\Services\ServiceStatus;

class BranchOfficeRepo
{
    /**
     * @var ServiceStatus
     */
    private $serviceStatus;

    public function getModel()
    {
        return new BranchOffice();
    }

    public function __construct(ServiceStatus $serviceStatus)
    {
        $this->serviceStatus = $serviceStatus;
    }

    public function nuevo($name)
    {
        $branchOffice = BranchOffice::create([
            'name' => $name,
            'status' => 1
        ]);
        if (!empty($branchOffice))
        {
            return ["success"=>"success","msg"=>"Registro guardado"];
        }
        return ["success"=>"error","msg"=>"Ah ocurrido un error"];
    }

    public function update($data)
    {
        $branchOffice = BranchOffice::where('id',$data['id'])->first();
        $branchOffice->name = $data['name'];
        if ($branchOffice->save())
        {
            return ["success"=>"success","msg"=>"Registro actualizado"];
        }
        return ["success"=>"error","msg"=>"Ah ocurrido un error"];
    }

    public function changeStatus($id)
    {
        $branchOffice = BranchOffice::where('id',$id)->first();
        $branchOffice->status = $this->serviceStatus->changeStatus($branchOffice->status);
        if ($branchOffice->save())
        {
            return ["success"=>"success","msg"=>"Registro actualizado"];
        }
        return ["success"=>"error","msg"=>"Ah ocurrido un error"];
    }
}