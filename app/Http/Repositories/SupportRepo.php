<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-02-08
 * Time: 01:27
 */

namespace App\Http\Repositories;


use App\Http\Entities\Attachments;
use App\Http\Entities\Calificacion;
use App\Http\Entities\Comments;
use App\Http\Entities\FileTickets;
use App\Http\Entities\HistorialAsignacion;
use App\Http\Entities\HistoryStatus;
use App\Http\Entities\HistoryTicket;
use App\Http\Entities\Problema;
use App\Http\Entities\Support;
use App\Mail\SendCommentMail;
use App\Mail\SendMailTicketByAdmin;
use App\Mail\SendTicketSupport;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
class SupportRepo
{

        public function getModel()
        {
            return new Support();
        }

        public function saveFiles($request)
        {

        }

        public function save($request)
        {
            $user = User::where('id',Auth::id())->first() ;
            $problema = Problema::where('id',$request['problema_id'])->first();
            $ticket =  Support::create([
                'user_id'=>Auth::id(),
                'telefono'=>$request['telefono'],
                'horario_contacto'=>$request['hora_inicio']." a ".$request['hora_fin'],
                'problema_id'=>$request['problema_id'],
                'descripcion'=>$request['descripcion'],
                'teamviewer_id'=>$request['teamviewer_id'],
                'teamviewer_password'=>$request['teamviewer_password'],
                'sucursal_id'=>$user->branchOffice_id,
                'department_id'=>$user->department_id,
                'fecha'=>date('Y-m-d H:i:s'),
                'contacto'=>$request['contacto'],
                'status'=>"Enviado",
                'slug' => Str::slug($problema->problema.date('YmdH:i:s')),
                'itemcode' =>"tk-".date('mdHis')
            ]);

            HistoryTicket::create([
                'slug' => $ticket->slug,
                'fecha'=> $ticket->fecha,
                'status' =>'Creado'
            ]);


            if (!empty($ticket))
            {
                $this->sendMail($ticket);
                if (!empty($request['files']))
                {
                    $uploads_dir = 'files/';

                    foreach ($request['files'] as $file)
                    {
                        $slugFile = Str::slug($ticket->id.date('YmdH:i:s'));
                        FileTickets::create([
                            'support_id' => $ticket->id,
                            'name' => $slugFile.$file->getClientOriginalName(),
                            'path' =>$uploads_dir,

                        ]);
                        move_uploaded_file($file, $uploads_dir.$slugFile.$file->getClientOriginalName());
                    }
                }
                return ['success'=>'success','msg'=>'Tu ticket se ha enviado '];
            }
            return ['success'=>'error','msg'=>'Ah ocurrido un error '];
        }

        public function sendMail($ticket)
        {
            $administradores = User::where('type','ti')->get();
            $user = User::where('id',Auth::id())->first();
            $ticketDetail = Support::with('sucursalTicket','ticketsProblemas')->where('id',$ticket->id)->first();
            $data['userName'] = $user->name;
            $data['sucursal'] = $ticketDetail->sucursalTicket->name;
            $data['problema'] = $ticketDetail->ticketsProblemas->problema;
            $data['descripcion'] = $ticketDetail->descripcion;
            $data['id']= $ticketDetail->id;
            $data['slug'] = $ticketDetail->slug;
            foreach ($administradores as $admin)
            {
                Mail::to($admin->email)->send(new SendTicketSupport($data));
            }

        }

        public function saveComment($data)
        {
            if (!isset($data['solucion']))
            {
                $data['type']="Comment";
            }else{
                $data['type']="Solucion";
            }

            $ticket = Support::with('usersTicket')->where('slug',$data['slug'])->first();

            $comment = Comments::create([
                'slug' => $data['slug'],
                'comment' => $data['comment'],
                'fecha' => date('Y-m-d H:i:s'),
                'user_id' => Auth::id()
            ]);

            if (!empty($data['solucion']))
            {
                $comment = Support::where('slug',$comment->slug)->first();
                $comment->solucion = $data['comment'];
                $comment->save();

                $ticket->status = "Pendiente finalizar";
                $ticket->comment_ticket_finalized = "por admin";
                $ticket->save();
                HistoryTicket::create([
                    'slug' => $data['slug'],
                    'fecha' => date('Y-m-d H:i:s'),
                    'status' => "Pendiente finalizar"
                ]);

            }
            if (!empty($comment))
            {
                $user = User::where('id',$data['user_id'])->first();

                if ($user->type == "ti"){
                    Mail::to($ticket->usersTicket->email)->send(new SendCommentMail($user,$ticket,$data['comment']));
                }else{
                    $userAdmin = HistorialAsignacion::with('usersHistorial')->where('slug',$data['slug'])->latest()->first();
                    if (!empty($userAdmin->usersHistorial->email))
                    {
                        Mail::to($userAdmin->usersHistorial->email)->send(new SendCommentMail($user,$ticket,$comment->comment));
                    }

                }

                return redirect()->route('view-ticket', ['slug' => $data['slug']]);
            }
        }


        public function saveAssigned($slug)
        {

            $ticket = Support::where('slug',$slug)->first();

            HistorialAsignacion::create([
                'slug'=>$slug,
                'assigned_user' => Auth::id(),
                'fecha' => date('Y-m-d H:i:s'),
                'typo' => 'assigned',
                'user_admin_reassigned' => Auth::id()
            ]);
            HistoryTicket::create([
               'slug'=>$slug,
               'fecha' => date('Y-m-d H:i:s'),
                'status'=> "Recibido"
            ]);
            $ticket->status = "Recibido";
            $ticket->save();
        }

    /**
     * Guarda la asignacion a un usuario hecha por el
     *administrador del sistema
     */
        public function saveAssignedByAdmin($ticket,$user)
        {
            $ticket = Support::where('slug',$ticket->slug)->first();
            $ticket->status = "Recibido";
            $ticket->save();

            $asignado = HistorialAsignacion::where('slug',$ticket->slug)
                ->where('type','assigned')->first();

            if (!empty($asignado))
            {
                $asignado->type = "reassigned";
                $asignado->save();
                $status_assigned = "reassigned";
            }else{
                $status_assigned = "assigned";

        }
            $nuevo = HistorialAsignacion::create([
                'slug'=>$ticket->slug,
                'assigned_user' => $user,
                'fecha' => date('Y-m-d H:i:s'),
                'typo' => 'assigned',
                'user_admin_reassigned' => Auth::id()
            ]);

            HistoryTicket::create([
                'slug' => $ticket->slug,
                'fecha' => date('Y-m-d H:i:s'),
                'status' => $status_assigned,

            ]);

            if (!empty($nuevo))
            {
                return ['success'=>'success','msg'=>'Usuario asignado correctamente'];
            }
            return ['success'=>'error','msg'=>'Ocurrio un error,consulte a su administrador'];

        }

        /**
         * Cambia el status de el ticker a finalizado
         *asi como tambien crea en el historico del ticket un nuevo registro
         */

        public function finalize($slug)
        {
            $ticket = Support::where('slug',$slug)->first();
            $ticket->status ="Finalizado";
            $ticket->save();

            HistoryTicket::create([
                'slug' => $slug,
                'fecha' => date('Y-m-d H:i:s'),
                'status' => "Finalizado"
            ]);

            return ['success'=> 'success','msg'=>'Ticket finalizado'];
        }

        /**
         * Finaliza el ticket desde la vista del correo
         */

        public function finalizeTicket($slug)
        {
            return view('support.finish',compact('slug'));
        }

        /**
         * Guarda la calificacion otorgada por el usuario
         *
         */

        public function saveCalif($data)
        {
            $ticket = Support::where('slug',$data['slug'])->first();
            Calificacion::create([
                'slug' => $data['slug'],
                'ticket_id' => $ticket->id,
                'user' =>Auth::id(),
                'resp1' => $data['satisfecho_atencion'],
                'resp2' => $data['rapidez_solucion'],
                'resp3' => $data['atencion_soporte'],
            ]);

            $ticket->status ="Finalizado";
            $ticket->save();

            HistoryTicket::create([
                'slug' => $data['slug'],
                'fecha' => date('Y-m-d H:i:s'),
                'status' => "Finalizado"
            ]);

        }

        /**
         *
         */
        public function saveAddFiles($request)
        {
            $ticket = Support::where('slug',$request['slug'])->first();
            if (!empty($request['addfiles']))
            {
                $uploads_dir = 'files/';

                foreach ($request['addfiles'] as $file)
                {
                    $slugFile = Str::slug($ticket->id.date('YmdH:i:s'));
                    FileTickets::create([
                        'support_id' => $ticket->id,
                        'name' => $slugFile.$file->getClientOriginalName(),
                        'path' =>$uploads_dir,

                    ]);
                    move_uploaded_file($file, $uploads_dir.$slugFile.$file->getClientOriginalName());
                }
            }
            return ['success'=>'success','msg'=>'Archivos agregados correctamente'];
        }

        public function saveTicketAdmin($data)
        {
            $user = User::where('id',$data['usuario_id'])->first();
            $admin = User::where('id',Auth::id())->first();
            $problema = Problema::where('id',$data['problema_id'])->first();
            $ticket =  Support::create([
                'user_id'=>$user->id,
                'telefono'=>$data['telefono'],
                'horario_contacto'=>"9:00 a 20:00",
                'problema_id'=>$data['problema_id'],
                'descripcion'=>$data['descripcion'],
                'teamviewer_id'=>$data['teamviewer_id'],
                'teamviewer_password'=>$data['teamviewer_password'],
                'sucursal_id'=>$user->branchOffice_id,
                'department_id'=>$user->department_id,
                'fecha'=>date('Y-m-d H:i:s'),
                'contacto'=>$data['contacto'],
                'status'=>"En proceso",
                'slug' => Str::slug($problema->problema.date('YmdH:i:s')),
                'itemcode' =>"tk-".date('mdHis'),
                'user_create_ticket_alter' => Auth::id()
            ]);

            HistoryTicket::create([
                'slug' => $ticket->slug,
                'fecha'=> $ticket->fecha,
                'status' =>'En proceso'
            ]);

            HistorialAsignacion::create([
                'slug'=>$ticket->slug,
                'assigned_user' => Auth::id(),
                'fecha' => date('Y-m-d H:i:s'),
                'typo' => 'assigned',
                'user_admin_reassigned' => Auth::id()
            ]);

            Mail::to($user->email)->send(new SendMailTicketByAdmin($user->name,$ticket,$admin->name,$problema->problema));
            return ['success'=>'success','msg'=>'El ticket fue creado correctamente'];
        }
}
