<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-01-26
 * Time: 02:50
 */

namespace App\Http\Repositories;


use App\Http\Entities\Department;
use App\Http\Services\ServiceStatus;

class DepartmentRepo
{
    /**
     * @var ServiceStatus
     */
    private $serviceStatus;

    public function getModel()
    {
        return new Department();
    }

    public function __construct(ServiceStatus $serviceStatus)
    {
        $this->serviceStatus = $serviceStatus;
    }

    public function create($name)
    {
        $department = Department::create([
           'name' => $name,
           'status' => 1
        ]);
        if (!empty($department))
        {
            return['success'=>'success','msg'=>'Registro creado correctamente'];
        }
        return['success'=>'success','msg'=>'Ocurrio un error , consulte a su administrador'];
    }

    public function update($data)
    {
        $department = Department::where('id',$data['id'])->first();
        $department->name = $data['name'];
        if ($department->save())
        {
            return['success'=>'success','msg'=>'Registro actualizado correctamente'];
        }
        return['success'=>'success','msg'=>'Ocurrio un error , consulte a su administrador'];
    }

    public function changeStatus($id)
    {
        $department = Department::where('id',$id)->first();
        $department->status = $this->serviceStatus->changeStatus($department->status);
        if ($department->save()){
            return['success'=>'success','msg'=>'Registro actualizado correctamente'];
        }
        return['success'=>'success','msg'=>'Ocurrio un error , consulte a su administrador'];
    }
}