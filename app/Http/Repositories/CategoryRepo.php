<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-02-06
 * Time: 13:21
 */

namespace App\Http\Repositories;


use App\Http\Entities\Category;

class CategoryRepo
{
    public function getModel()
    {
        return new Category();
    }

    public function save($data)
    {
        $category = Category::create([
            'name' => $data['name'],
            'description' => $data['description']
        ]);

        if (!empty($category))
        {
            return ['success'=>'success','msg'=>'Categoria creada'];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error'];
    }

    public function update($data)
    {
        $category = Category::where('id',$data['id'])->first();
        $category->name = $data['name'];
        $category->description =$data['description'];
        if ($category->save())
        {
            return ['success'=>'success','msg'=>'Categoria actualizada'];
        }
        return ['success'=>'error','msg'=>'Ah ocurrido un error'];
    }
}