<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-01-19
 * Time: 11:10
 */

namespace App\Http\Repositories;


use App\Http\Entities\BranchOffice;
use App\Http\Entities\Department;
use App\Http\Request\EditUserRequest;
use App\Http\Services\ServiceStatus;
use App\User;
use Intervention\Image\ImageManagerStatic as Image;
use Caffeinated\Shinobi\Concerns\HasRolesAndPermissions;

class UserRepo
{
    /**
     * @var ServiceStatus
     */
    private $serviceStatus;

    public function __construct(ServiceStatus $serviceStatus)
    {
        $this->serviceStatus = $serviceStatus;
    }

    public function save($data)
    {
        $branchOffice = BranchOffice::where('id',$data['branchOffice_id'])->first();
        $department = Department::where('id',$data['department_id'])->first();

        if (empty($branchOffice))
        {
            $branchOffice = BranchOffice::create([
                'name'=>$data['branchOffice_id'],
                'status'=>1
            ]);
            $data['branchOffice_id'] = $branchOffice->id;
        }

        if (empty($department))
        {
            $department = Department::create([
                'name'=>$data['department_id'],
                'status'=>1
            ]);
            $data['department_id'] = $department->id;
        }

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'status' => 1,
            'branchOffice_id' => $data['branchOffice_id'],
            'department_id' => $data['department_id'],
            'type'=>$data['type'],
            'imagen'=>'/images/users/',
            'imagen_name'=>'profile.png'
            ]);
        if (!$user)
        {
            return ['success'=>'error','msg'=>'Ocurrio un error consulta con tu administrador'];
        }

        $user->assignRoles($data['type']);
        return ['success'=>'success','msg'=>'Registro guardado'];
    }

    public function update($data)
    {
        $branchOffice = BranchOffice::where('id',$data['branchOffice_id'])->first();
        $department = Department::where('id',$data['department_id'])->first();

        if (empty($branchOffice))
        {
            $branchOffice = BranchOffice::create([
                'name'=>$data['branchOffice_id'],
                'status'=>1
            ]);
            $data['branchOffice_id'] = $branchOffice->id;
        }

        if (empty($department))
        {
            $department = Department::create([
                'name'=>$data['department_id'],
                'status'=>1
            ]);
            $data['department_id'] = $department->id;
        }

        $user = User::where('id',$data['id'])->first();
        $user->name=$data['name'];
        $user->email=$data['email'];
        $user->branchOffice_id = $data['branchOffice_id'];
        $user->department_id = $data['department_id'];
        $user->type = $data['type'];
        $user->syncRoles($data['type']);
        if ($user->save())
        {
            return ['success'=>'success','msg'=>'Registro actualizado'];
        }
        return ['success'=>'error','msg'=>'Ocurrio un error consulta con tu administrador'];

    }

    public function updateUser($data)
    {
        $user = User::where('id',$data['id'])->first();
        $user->name=$data['name'];
        $user->email=$data['email'];
        $user->save();
        return back()->with('success','Tus datos han sido actualizados');
    }

    public function changePassword($data)
    {
        $user = User::where('id',$data['id'])->first();
        $user->password = bcrypt($data['password']);

        if ($user->save()){
            return ['success'=>'success','msg'=>'Tu contraseña fue actualizada'];
        }
        return ['success'=>'error','msg'=>'Ocurrio un error consulta con tu administrador'];

    }

    public function changeStatus($id)
    {
        $user = User::where('id',$id)->first();
        $user->status = $this->serviceStatus->changeStatus($user->status);
        if ($user->save()){
            return ['success'=>'success','msg'=>'Estatus actualizado'];
        }
        return ['success'=>'error','msg'=>'Ocurrio un error consulta con tu administrador'];
    }
}