<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-02-21
 * Time: 13:48
 */

namespace App\Http\Entities;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class TicketsPendiente extends Model
{
    use Notifiable;

     protected $fillable = [
         'fecha','motivo','slug'
     ];
}