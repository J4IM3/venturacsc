<?php

namespace App\Http\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Comments extends Model
{
    use Notifiable;

    protected $fillable = [
        'slug','comment','fecha','user_id','type'
    ];

    public function usersComments()
    {
        return $this
            ->hasOne(User::class,'id','user_id');
    }
}
