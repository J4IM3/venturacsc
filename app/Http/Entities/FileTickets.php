<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class FileTickets extends Model
{
    use Notifiable;

    protected $fillable=[
        'id','support_id','name','path'
    ];

    public function ticketFile()
    {
        return $this
            ->belongsTo(FileTickets::class);
    }
}
