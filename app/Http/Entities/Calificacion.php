<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Calificacion extends Model
{
    protected $fillable = [
        'ticket_id','slug','resp1','resp2','resp3','user'
    ];
}
