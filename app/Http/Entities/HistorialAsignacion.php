<?php

namespace App\Http\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class HistorialAsignacion extends Model
{
    use Notifiable;

    protected $fillable = [
        'slug','assigned_user','fecha','type','user_admin_reassigned'
    ];

    public function usersHistorial()
    {
        return $this
            ->hasOne(User::class,'id','assigned_user');
    }

    public function history_assignations()
    {
        return $this
            ->hasOne(User::class,'id','assigned_user');
    }

    public function history_assigned()
    {
        return $this->belongsTo(Support::class);
    }
}
