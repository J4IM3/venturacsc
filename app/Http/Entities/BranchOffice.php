<?php

namespace App\Http\Entities;

use App\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class BranchOffice extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','status'
    ];

    public function sucursalTicket()
    {
        return $this->belongsTo(BranchOffice::class);
    }

    public function userSucursal()
    {
        return $this
            ->belongsTo(User::class);
    }
}
