<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-02-18
 * Time: 00:48
 */

namespace App\Http\Entities;


use Illuminate\Database\Eloquent\Model;

class HistoryTicket extends Model
{
    protected $fillable = [
        'slug','fecha','status'
    ];
}