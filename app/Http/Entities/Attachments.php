<?php

namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Attachments extends Model
{
    use Notifiable;

    protected $fillable=[
        'id','support_id','name','path'
    ];
}
