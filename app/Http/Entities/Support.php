<?php

namespace App\Http\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Support extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','telefono','horario_contacto','problema_id','descripcion','solucion','teamviewer_id','teamviewer_password','sucursal_id','department_id','fecha','status','slug','itemcode','contacto','user_create_ticket_alter','comment_ticket_finalized'
    ];

    public function usersTicket()
    {
        return $this
            ->hasOne(User::class,'id','user_id');
    }

    public function sucursalTicket()
    {
        return $this
            ->hasOne(BranchOffice::class,'id','sucursal_id');
    }

    public function department_ticket()
    {
        return $this
            ->hasOne(Department::class,'id','department_id');
    }

    public function history_assigned()
    {
        return $this
            ->hasMany(HistorialAsignacion::class,'slug','slug');
    }

    public function ticketsProblemas()
    {
        return $this
            ->hasOne(Problema::class,'id','problema_id');
    }

    public function ticketFile()
    {
        return $this
            ->hasMany(FileTickets::class,'support_id','id');
    }
}
