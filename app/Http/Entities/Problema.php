<?php

namespace App\Http\Entities;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Problema extends Model
{
    use Notifiable;

    protected $fillable = [
        'problema','status'
    ];

    public function ticketsProblemas()
    {
        return $this
            ->belongsTo(Support::class);
    }
}
